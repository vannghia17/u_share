import 'package:flutter/material.dart';

enum TabItem { message, news, setting, roombook }

class TabHelper {
  static int index(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.message:
        return 0;
      case TabItem.news:
        return 1;
      case TabItem.roombook:
        return 2;
      case TabItem.setting:
        return 3;
    }
    return 0;
  }

  static TabItem item(int index) {
    switch (index) {
      case 0:
        return TabItem.message;
      case 1:
        return TabItem.news;
      case 2:
        return TabItem.roombook;
      case 3:
        return TabItem.setting;
    }
    return TabItem.message;
  }

  static String description(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.message:
        return 'Tin nhắn';
      case TabItem.news:
        return 'Bảng tin';
      case TabItem.setting:
        return 'Cài đặt';
      case TabItem.roombook:
        return  'Phòng họp';
    }
    return 'Khác';
  }

  static IconData icon(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.message:
        return Icons.message;
      case TabItem.news:
        return Icons.book;
      case TabItem.setting:
        return Icons.settings;
      case TabItem.roombook:
        return Icons.local_library;
    }
    return Icons.layers;
  }

  static MaterialColor color(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.message:
        return Colors.blue;
      case TabItem.news:
        return Colors.green;
      case TabItem.setting:
        return Colors.red;
      case TabItem.roombook:
        return Colors.deepPurple;
    }
    return Colors.grey;
  }
}
