import 'dart:math';
import 'package:flutter/material.dart';

class Randomizer {

  static Random random = new Random();

  static final tableChar = ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M',
  'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'];

  static String getRandomString() {
    int pos1 = random.nextInt(tableChar.length - 1);
    int pos2 = random.nextInt(tableChar.length - 1);
    int pos3 = random.nextInt(tableChar.length - 1);
    int pos4 = random.nextInt(tableChar.length - 1);

    return tableChar[pos1] + tableChar[pos2] + tableChar[pos3] + tableChar[pos4];
  }

  static getRandomColors() {
    int pos = random.nextInt(8);
    switch(pos) {
      case 0:
        return Colors.red;
      case 1:
        return Colors.green;
      case 2:
        return Colors.blue;
      case 3:
        return Colors.pink;
      case 4:
        return Colors.purple;
      case 5:
        return Colors.brown;
      case 6:
        return Colors.yellow;
      case 7:
        return Colors.grey;
      default:
        return Colors.blue;
    }
  }

}