class InfoMapper {
  static const departments = {
    'CNTT': 'Công nghệ thông tin',
  };

  static String departmentName(String department) {
    return departments[department];
  }
}
