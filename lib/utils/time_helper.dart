import 'package:flutter/material.dart';

class ShiftTime {
  static const SHIFT_1_BEGIN = TimeOfDay(hour: 7, minute: 0);
  static const SHIFT_1_END = TimeOfDay(hour: 8, minute: 30);

  static const SHIFT_2_BEGIN = TimeOfDay(hour: 8, minute: 45);
  static const SHIFT_2_END = TimeOfDay(hour: 10, minute: 15);

  static const SHIFT_3_BEGIN = TimeOfDay(hour: 10, minute: 30);
  static const SHIFT_3_END = TimeOfDay(hour: 12, minute: 0);

  static const SHIFT_4_BEGIN = TimeOfDay(hour: 13, minute: 0);
  static const SHIFT_4_END = TimeOfDay(hour: 14, minute: 30);

  static const SHIFT_5_BEGIN = TimeOfDay(hour: 14, minute: 45);
  static const SHIFT_5_END = TimeOfDay(hour: 16, minute: 15);

  static const SHIFT_6_BEGIN = TimeOfDay(hour: 16, minute: 30);
  static const SHIFT_6_END = TimeOfDay(hour: 18, minute: 00);

  static const SHIFT_BEGIN = [
    null,
    SHIFT_1_BEGIN,
    SHIFT_2_BEGIN,
    SHIFT_3_BEGIN,
    SHIFT_4_BEGIN,
    SHIFT_5_BEGIN,
    SHIFT_6_BEGIN,
  ];

  static const SHIFT_END = [
    null,
    SHIFT_1_END,
    SHIFT_2_END,
    SHIFT_3_END,
    SHIFT_4_END,
    SHIFT_5_END,
    SHIFT_6_END,
  ];
}
