import 'package:cloud_firestore/cloud_firestore.dart';

CollectionReference getAllUsers() {
  return Firestore.instance.collection('users');
}

DocumentReference getUser({String id}) {
  return Firestore.instance.collection('users').document(id);
}

DocumentReference getGroupInfo({String id}) {
  return Firestore.instance.collection('groups').document(id);
}

CollectionReference getP2PMessagesCollection() {
  return Firestore.instance.collection('p2p-messages');
}

CollectionReference getSingleChatMessages({String id}) {
  return Firestore.instance
      .collection('p2p-messages')
      .document(id)
      .collection(id);
}

CollectionReference getGroupChatMessage({String id}) {
  return Firestore.instance
      .collection('group-messages')
      .document(id)
      .collection(id);
}

Query getAllGroups() {
  return Firestore.instance.collection('groups');
}

Query getGroupsByUsername({String username}) {
  return Firestore.instance
      .collection('groups')
      .where('members', arrayContains: username);
}

Query getNews() {
  return Firestore.instance.collection('news');
}

Query getNewsFromUserGroupId({String userGroupId}) {
  return Firestore.instance
      .collection('news')
      .where('groups', isEqualTo: userGroupId);
}

void pinToEachOther(String id1, String id2) {
  Firestore.instance.collection('users').document(id1).updateData({
    'pinned': FieldValue.arrayUnion([id2]),
  });
  Firestore.instance.collection('users').document(id2).updateData({
    'pinned': FieldValue.arrayUnion([id1]),
  });
}

CollectionReference getAllBuildings() {
  return Firestore.instance.collection('rooms');
}

CollectionReference getAllRoomBooking() {
  return Firestore.instance.collection('room_booking');
}

Query getAllBookingNoti({String username}) {
  return Firestore.instance.collection('room_booking_noti').where('user', isEqualTo: username);
}

CollectionReference getAdminChatMessages({String id}) {
  return Firestore.instance
      .collection('w2m-messages')
      .document(id)
      .collection(id);
}