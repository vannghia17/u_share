import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:u_share/utils/apis.dart';
import 'package:u_share/dto/user.dart';

class UserRepository {
  User currentUser;

  UserRepository() {
    this.currentUser = null;
  }

  Future<Map<String, String>> authenticate({
    @required String username,
    @required String password,
  }) async {
    var response = await http.post(uShareServerAddressAuth,
        body: {'username': username, 'password': password});

    if (response.statusCode == 200) {
      Map<String, dynamic> resJson = json.decode(response.body);

      if (resJson['status'] == 'success') {
        currentUser = User.fromJson(resJson['data']);
      }

      return {'username': resJson['username'], 'status': resJson['status']};
    } else {
      return {'username': username, 'status': 'error'};
    }
  }

  Future<bool> isLoggedIn() async {
    await Future.delayed(Duration(seconds: 1));
    return this.currentUser != null;
  }

  Future<void> removeCurrentUser() async {
    await Future.delayed(Duration(seconds: 1));
    this.currentUser = null;
  }

  Future<void> makeColor() async {
    await Future.delayed(Duration(seconds: 1));
  }
}
