import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:u_share/blocs/authentication/authentication.dart';
import 'package:u_share/repositories/user_repository.dart';
import 'package:u_share/screens/login/login_screen.dart';
import 'package:u_share/screens/main/main_screen.dart';
import 'package:u_share/screens/common/splash_screen.dart';
import 'package:u_share/screens/common/loading_indicator.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  BlocSupervisor().delegate = SimpleBlocDelegate();

  runApp(UShare(
    userRepository: UserRepository(),
  ));
}

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onTransition(Transition transition) {
    super.onTransition(transition);
    print(transition);
  }

  @override
  void onError(Object error, StackTrace stackTrace) {
    super.onError(error, stackTrace);
    print(error);
  }
}

class UShare extends StatefulWidget {
  final UserRepository userRepository;

  UShare({Key key, @required this.userRepository}) : super(key: key);

  @override
  _UShareState createState() => _UShareState();
}

class _UShareState extends State<UShare> {
  AuthenticationBloc _authenticationBloc;
  UserRepository get _userRepository => widget.userRepository;

  @override
  void initState() {
    _authenticationBloc = AuthenticationBloc(userRepository: _userRepository);
    _authenticationBloc.dispatch(AppStarted());
    super.initState();
  }

  @override
  void dispose() {
    _authenticationBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthenticationBloc>(
      bloc: _authenticationBloc,
      child: MaterialApp(
        localizationsDelegates: [GlobalMaterialLocalizations.delegate],
        supportedLocales: [Locale.fromSubtags(languageCode: 'vi')],
        title: 'u_share for HCMUS',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        debugShowCheckedModeBanner: false,
        home: BlocBuilder<AuthenticationEvent, AuthenticationState>(
          bloc: _authenticationBloc,
          builder: (BuildContext context, AuthenticationState state) {
            if (state is AuthenticationUninitialized) {
              return SplashScreen();
            } else if (state is AuthenticationAuthenticated) {
              return MainScreen(userRepository: _userRepository);
            } else if (state is AuthenticationUnauthenticated) {
              return LoginScreen(userRepository: _userRepository);
            } else if (state is AuthenticationLoading) {
              return LoadingIndicator();
            }
          },
        ),
      ),
    );
  }
}
