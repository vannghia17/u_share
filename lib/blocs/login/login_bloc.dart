import 'dart:async';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:u_share/repositories/user_repository.dart';
import 'package:u_share/blocs/authentication/authentication.dart';
import 'package:u_share/blocs/login/login.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({
    @required this.userRepository,
    @required this.authenticationBloc,
  })  : assert(userRepository != null),
        assert(authenticationBloc != null);

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoading();

      try {
        final result = await userRepository.authenticate(
          username: event.username,
          password: event.password,
        );

        if (result['status'] == 'success') {
          authenticationBloc.dispatch(LoggedIn());
          yield LoginInitial();
        }

        if (result['status'] == 'failed') {
          yield LoginFailure(error: 'Username and Password wrong.');
        }

        if (result['status'] == 'user_not_found') {
          yield LoginFailure(error: 'User not found.');
        }

        if (result['status'] == 'error') {
          yield LoginFailure(error: 'Error when login. Please check network.');
        }
      } catch (error) {
        yield LoginFailure(error: error.toString());
      }
    }
  }
}
