import 'dart:async';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:u_share/blocs/authentication/authentication.dart';
import 'package:u_share/repositories/user_repository.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository userRepository;

  AuthenticationBloc({@required this.userRepository})
      : assert(userRepository != null);

  @override
  AuthenticationState get initialState => AuthenticationUninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is AppStarted) {
      final bool isLoggedIn = await userRepository.isLoggedIn();

      if (isLoggedIn) {
        yield AuthenticationAuthenticated();
      } else {
        yield AuthenticationUnauthenticated();
      }
    }

    if (event is LoggedIn) {
      yield AuthenticationLoading();
      await userRepository.makeColor();
      yield AuthenticationAuthenticated();
    }
    
    if (event is LoggedOut) {
      yield AuthenticationLoading();
      await userRepository.removeCurrentUser();
      yield AuthenticationUnauthenticated();
    }
  }
}
