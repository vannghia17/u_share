class RoomBookingNoti {
  String user;
  String idRoom;
  int shift;
  DateTime date;
  bool success;
  DateTime timeStamp;
  String message;

  RoomBookingNoti({this.user, this.idRoom, this.shift, this.date, this.success, this.timeStamp, this.message});

  RoomBookingNoti.fromJson(Map<String, dynamic> json) {
    this.user = json['user'];
    this.idRoom = json['idRoom'];
    this.shift = json['shift'];
    this.date = json['date'];
    this.success = json['success'];
    this.timeStamp = json['timeStamp'];
    this.message = json['message'];
  }
}