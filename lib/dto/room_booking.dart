class RoomBooking {
  String user;
  String idRoom;
  int shift;
  DateTime date;
  int status;
  DateTime timeRequest;

  RoomBooking({this.user, this.idRoom, this.shift, this.status});

  RoomBooking.fromJson(Map<String, dynamic> json) {
    this.user = json['user'];
    this.idRoom = json['idRoom'];
    this.shift = json['shift'];
    this.date = json['date'];
    this.status = json['status'];
    this.timeRequest = json['timeRequest'];
  }

  @override
  String toString() {
    return '$user - $idRoom - ${date.day}/${date.month} - ($shift) - $status';
  }
}