class New {
  String title;
  String content;
  String groups;
  String time;
  String place;
  DateTime timeStamp;
  bool isPublic;
  bool isImportant;

  New({this.title, this.content, this.groups, this.time, this.place, this.timeStamp, this.isPublic, this.isImportant});

  New.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    content = json['content'];
    groups = json['groups'];
    time = json['time'];
    place = json['place'];
    timeStamp = json['timeStamp'];
    isPublic = json['isPublic'];
    isImportant = json['isImportant'];
  }

  @override
  String toString() {
    return '$title - $groups';
  }
}