class Building {
  String idBuilding;
  List<Room> rooms;

  Building({this.idBuilding, this.rooms});

  Building.fromJson(Map<String, dynamic> json) {
    this.idBuilding = json['idBuilding'];
    rooms = json['rooms']?.map<Room>((x) => Room.fromJson(x))?.toList();
  }
}

class Room {
  String idRoom;
  int capacity;
  bool airCondition;
  bool projector;

  Room({this.idRoom, this.capacity, this.airCondition, this.projector});

  Room.fromJson(Map<dynamic, dynamic> json) {
    this.idRoom = json['idRoom'];
    this.capacity =  json['capacity'];
    this.airCondition = json['airCondition'];
    this.projector = json['projector'];
  }

  @override
  String toString() {
    return '$idRoom:$capacity';
  }
}