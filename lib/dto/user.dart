class User {
  String username;
  String name;
  String department;
  String sex;
  String avatar;

  User({this.username, this.name, this.department, this.sex, this.avatar});

  User.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    name = json['name'];
    department = json['department'];
    sex = json['sex'];
    avatar = json['avatar'];
  }

  @override
  String toString() {
    return '$username - $name - $department - $sex';
  }
}
