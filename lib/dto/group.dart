class Group {
  String groupId;
  String groupName;
  String groupDescription;
  String groupImage;
  List<dynamic> members;

  Group({this.groupId, this.groupName, this.groupDescription, this.groupImage, this.members});

  Group.fromJson(Map<String, dynamic> json) {
    groupId = json['groupId'];
    groupName = json['groupName'];
    groupDescription = json['groupDescription'];
    groupImage = json['groupImage'];
    members = json['members'].map((x) => x.toString()).toList();
  }

  @override
  String toString() {
    return '$groupId - $groupName';
  }
}