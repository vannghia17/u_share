enum MessageItemType { user, group}

class MessageListItem {
  MessageItemType type;
  String title;
  String subTitle;
  String image;
  Map<String, dynamic> data;

  MessageListItem({this.type, this.title, this.subTitle, this.image, this.data});

  MessageListItem.userItem(Map<String, dynamic> json) {
    this.type = MessageItemType.user;
    this.title = json['name'];
    this.subTitle = json['username'];
    this.image = json['avatar'];
    this.data = json;
  }

  MessageListItem.groupItem(Map<String, dynamic> json) {
    this.type = MessageItemType.group;
    this.title = json['groupName'];
    this.subTitle = json['groupDescription'];
    this.image = json['groupImage'];
    this.data = json;
  }
}
