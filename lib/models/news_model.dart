import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

class NewsItem {
  const NewsItem({
    @required this.content,
    @required this.title,
    @required this.groupsId,
    @required this.time,
    @required this.createdTime,
    @required this.isImportant,
    @required this.isPublic,
    @required this.place
  });

  final String content;
  final String title;
  final String groupsId;
  final bool isImportant;
  final bool isPublic;
  final String place;
  final String time;
  final DateTime createdTime;
}

class GroupInfo {
  const GroupInfo({
    @required this.groupName,
    @required this.groupDescription,
    @required this.groupId,
    @required this.groupAvatarUrl,
  });

  final String groupName;
  final String groupDescription;
  final String groupId;
  final String groupAvatarUrl;
}

NewsItem newsItemParser(DocumentSnapshot object) {
  return NewsItem(
    content: object['content'] == null || object['content'] == '' ? 'Empty content' : object['content'],
    title: object['title'] == null || object['title'] == '' ? 'Empty title' : object['title'],
    groupsId: object['groups'] ?? '',
    createdTime: DateTime.parse(object['timeStamp'].toString()), 
    isImportant: object['isImportant'] ?? false,
    isPublic: object['isPublic'] ?? false,
    place: object['place'] == null || object['place'] == '' ? 'Unknown' : object['place'],
    time: object['time']
  );
}