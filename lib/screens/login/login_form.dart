import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:u_share/blocs/login/login.dart';
import 'package:u_share/utils/utils.dart';

class LoginForm extends StatefulWidget {
  final LoginBloc loginBloc;

  LoginForm({
    Key key,
    @required this.loginBloc,
  }) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> with TickerProviderStateMixin {
  LoginBloc get _loginBloc => widget.loginBloc;

  bool _obsecureText = true;
  AnimationController _loginAnimationController;

  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  void _onLoginButtonPressed() {
    _loginAnimationController.forward();

    _loginBloc.dispatch(LoginButtonPressed(
      username: _usernameController.text,
      password: _passwordController.text,
    ));
  }

  @override
  void initState() {
    super.initState();
    _loginAnimationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 2),
    );
  }

  @override
  void dispose() {
    _loginAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginEvent, LoginState>(
      bloc: _loginBloc,
      builder: (BuildContext context, LoginState state) {
        if (state is LoginFailure) {
          _onWidgetDidBuild(() {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text('${state.error}'),
                backgroundColor: Colors.red,
              ),
            );
          });
        }
        return Form(
          child: Column(
            children: <Widget>[
              TextFormField(
                maxLines: 1,
                controller: _usernameController,
                cursorColor: PRIMARY_COLOR,
                decoration: const InputDecoration(
                  labelText: "Tài khoản",
                  labelStyle: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              SizedBox(height: 20),
              TextFormField(
                obscureText: _obsecureText,
                maxLines: 1,
                controller: _passwordController,
                cursorColor: PRIMARY_COLOR,
                decoration: InputDecoration(
                    suffixIcon: Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: IconButton(
                          onPressed: () {
                            setState(() {
                              _obsecureText = !_obsecureText;
                            });
                          },
                          icon: Icon(
                            _obsecureText
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                        )),
                    labelText: "Mật khẩu",
                    labelStyle: TextStyle(
                      fontSize: 16.0,
                    )),
              ),
              SizedBox(height: 50),
              Container(
                height: 50,
                // child: LoginButton(
                //   onLogin: state is !LoginLoading ? _onLoginButtonPressed : null,)
                child: RaisedButton(
                    onPressed:
                        state is! LoginLoading ? _onLoginButtonPressed : null,
                    color: PRIMARY_COLOR,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)),
                    child: Center(
                      child: Text(
                        'Đăng Nhập',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
              ),
              Container(
                child:
                    state is LoginLoading ? CircularProgressIndicator() : null,
              ),
            ],
          ),
        );
      },
    );
  }
}
