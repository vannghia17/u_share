import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:u_share/repositories/user_repository.dart';
import 'package:u_share/blocs/login/login.dart';
import 'package:u_share/blocs/authentication/authentication.dart';
import 'package:u_share/utils/utils.dart';
import 'login_form.dart';

class LoginScreen extends StatefulWidget {
  final UserRepository userRepository;

  LoginScreen({Key key, @required this.userRepository})
      : assert(userRepository != null),
        super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginBloc _loginBloc;
  AuthenticationBloc _authenticationBloc;

  UserRepository get _userRepository => widget.userRepository;

  @override
  void initState() {
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _loginBloc = LoginBloc(
      userRepository: _userRepository,
      authenticationBloc: _authenticationBloc,
    );
    super.initState();
  }

  @override
  void dispose() {
    _loginBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;

    return BlocBuilder<LoginEvent, LoginState>(
      bloc: _loginBloc,
      builder: (BuildContext context, LoginState state) {
        return Scaffold(
          resizeToAvoidBottomInset: true,
          body: ScrollConfiguration(
            behavior: NoGlowScrollBehavior(),
            child: SingleChildScrollView(
              child: Container(
                height: screenHeight,
                padding: const EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft,
                      child: SafeArea(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 20.0),
                          child: Hero(
                            tag: 'splash_image',
                            child: Image.asset(
                              'assets/images/logo.png',
                              width: 96.0,
                              height: 96.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Text(
                      "Ushare\nUniversity of Science",
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: PRIMARY_COLOR,
                        fontSize: 30
                      ),
                    ),
                    SizedBox(height: 20),
                    LoginForm(
                      loginBloc: _loginBloc,
                    ),
                    SizedBox(height: 100),
                  ],
                ),
              )
            ),
          )
        );
      },
    );
  }
}
