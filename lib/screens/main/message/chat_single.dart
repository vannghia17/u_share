import 'package:flutter/material.dart';
import 'package:u_share/dto/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:u_share/db/firestore_refs.dart' as refs;
import 'package:u_share/screens/main/message/message_factory.dart';
import 'package:u_share/screens/main/message/stickers.dart';
import 'package:u_share/screens/main/message/info.dart';

class ChatSingle extends StatefulWidget {
  final User user, peer;

  ChatSingle({Key key, @required this.user, @required this.peer})
      : super(key: key);

  @override
  _ChatSingleState createState() => _ChatSingleState();
}

class _ChatSingleState extends State<ChatSingle> {
  User get _user => widget.user;
  User get _peer => widget.peer;

  String p2pChatId = '';
  List<DocumentSnapshot> listMessages = List();

  bool isInputEmpty = true;
  bool isShowingStickers = false;
  final TextEditingController inputController = TextEditingController();
  final FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();

    getP2PChatId();

    inputController.addListener(() {
      if (inputController.text.trim() == '') {
        setState(() {
          isInputEmpty = true;
        });
      } else {
        setState(() {
          isInputEmpty = false;
        });
      }
    });

    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        setState(() {
          isShowingStickers = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              buildListMessages(),
              (isShowingStickers ? buildStickerPicker() : Container()),
              buildInput(),
            ],
          ),
        ],
      ),
    );
  }

  // Build UI
  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: Text(_peer.name),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.info,
          ),
          onPressed: () {
            Info.peerInfo(context: context, user: _peer);
          },
        ),
      ],
      centerTitle: true,
      elevation: 0.0,
      bottomOpacity: 0.5,
    );
  }

  Widget buildListMessages() {
    return Expanded(
      child: p2pChatId == ''
          ? Center(
              child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.blue)))
          : StreamBuilder(
              stream: refs
                  .getSingleChatMessages(id: p2pChatId)
                  .orderBy('time', descending: true)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return (Center(
                      child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.blue))));
                } else {
                  listMessages = snapshot.data.documents;

                  return ListView.builder(
                    itemBuilder: (context, index) {
                      return (listMessages[index].data['from'] ==
                              _user.username)
                          ? MessageFactory.me(
                              msg: listMessages[index].data,
                              isLast: isLastInChain(index))
                          : MessageFactory.peer(
                              name: _peer.name,
                              avatar: _peer.avatar,
                              msg: listMessages[index].data,
                              isFirst: isFirstInChain(index),
                              isLast: isLastInChain(index),
                            );
                    },
                    itemCount: listMessages.length,
                    reverse: true,
                    padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                  );
                }
              },
            ),
    );
  }

  Widget buildStickerPicker() {
    return Container(
      child: Wrap(
        spacing: 4.0,
        runSpacing: 4.0,
        children: qoobeeStickers.map((item) {
          return FlatButton(
            onPressed: () {
              sendMessage(content: item, type: 1);
            },
            child: Image.asset(
              'assets/stickers/$item',
              width: 48.0,
            ),
          );
        }).toList(),
      ),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: Colors.grey[400],
            width: 0.5,
          ),
        ),
      ),
      padding: EdgeInsets.all(8.0),
    );
  }

  Widget buildInput() {
    return Container(
      child: Row(
        children: <Widget>[
          // Material(
          //   child: Container(
          //     margin: EdgeInsets.symmetric(horizontal: 1.0),
          //     child: IconButton(
          //       icon: Icon(Icons.image),
          //       onPressed: () {},
          //       color: Colors.blue,
          //     ),
          //   ),
          //   color: Colors.white,
          // ),
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.0),
              child: IconButton(
                icon: Icon(Icons.announcement),
                onPressed: () {
                  focusNode.unfocus();
                  setState(() {
                    isShowingStickers = !isShowingStickers;
                  });
                },
                color: Colors.blue,
              ),
            ),
            color: Colors.white,
          ),
          Flexible(
            child: Container(
              child: TextField(
                style: TextStyle(
                  fontSize: 16.0,
                ),
                decoration: InputDecoration.collapsed(
                    hintText: ' Nhập nội dung tin nhắn ...',
                    hintStyle: TextStyle(color: Colors.grey[300])),
                focusNode: focusNode,
                controller: inputController,
              ),
            ),
          ),
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.0),
              child: IconButton(
                icon: Icon(Icons.send),
                onPressed: (isInputEmpty || p2pChatId == '')
                    ? null
                    : () => sendMessage(content: inputController.text, type: 0),
                color: Colors.blue,
              ),
            ),
            color: Colors.white,
          ),
        ],
      ),
      width: double.infinity,
      height: 50.0,
      decoration: BoxDecoration(
        border: Border(
            top: BorderSide(
          color: Colors.blue,
          width: 0.2,
        )),
        color: Colors.white,
      ),
    );
  }

  // Actions
  void getP2PChatId() async {
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      p2pChatId = _user.username.compareTo(_peer.username) < 0
          ? '${_user.username}:${_peer.username}'
          : '${_peer.username}:${_user.username}';
    });
  }

  void sendMessage({String content, int type}) {
    inputController.clear();

    int now = DateTime.now().millisecondsSinceEpoch;

    refs.getSingleChatMessages(id: p2pChatId).document(now.toString()).setData({
      'from': _user.username,
      'to': _peer.username,
      'type': type,
      'content': content,
      'time': now
    });

    refs.pinToEachOther(_user.username, _peer.username);
  }

  bool isLastInChain(int index) {
    return (index - 1 >= 0)
        ? (listMessages[index].data['from'] !=
                listMessages[index - 1].data['from']
            ? true
            : false)
        : true;
  }

  bool isFirstInChain(int index) {
    return (index + 1 < listMessages.length)
        ? (listMessages[index].data['from'] !=
                listMessages[index + 1].data['from']
            ? true
            : false)
        : true;
  }
}
