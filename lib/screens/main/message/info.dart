import 'package:flutter/material.dart';
import 'package:u_share/dto/user.dart';
import 'package:u_share/dto/group.dart';
import 'package:u_share/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import './chat_single.dart';

class Info {
  static peerInfo({BuildContext context, User user}) {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(user.name),
          contentPadding:
              EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.pop(context),
              child: Text('Đóng'),
            ),
          ],
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: NetworkImage(user.avatar),
                    radius: 32.0,
                  ),
                  SizedBox(
                    width: 16.0,
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('MSSV: ${user.username}'),
                        Text(
                            'Khoa: ${InfoMapper.departmentName(user.department)}'),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  static groupInfo(
      {BuildContext context,
      User user,
      Group group,
      Map<String, dynamic> members}) {
    List<dynamic> listMembers = List();
    members.forEach((key, value) {
      listMembers.add(value);
    });
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context2) {
        return AlertDialog(
          title: Text('${group.groupName} - Thành viên'),
          contentPadding: EdgeInsets.all(16.0),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.pop(context),
              child: Text('Đóng'),
            ),
          ],
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: listMembers.map((x) {
                      if (x.data['username'] == user.username)
                        return Container();
                      return ListTile(
                        onTap: () {
                          Navigator.pop(context2);
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ChatSingle(
                                      user: user,
                                      peer: User.fromJson(x.data),
                                    ),
                              ));
                        },
                        leading: CircleAvatar(
                          backgroundImage:
                              CachedNetworkImageProvider(x['avatar']),
                          radius: 18.0,
                        ),
                        title: Text(
                          x['name'],
                          style: TextStyle(fontSize: 14.0),
                        ),
                        trailing: Icon(Icons.arrow_right),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
