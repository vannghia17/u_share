import 'package:flutter/material.dart';
import 'package:u_share/dto/user.dart';
import 'package:u_share/dto/group.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:u_share/db/firestore_refs.dart' as refs;
import 'package:u_share/screens/main/message/message_factory.dart';
import 'package:u_share/screens/main/message/stickers.dart';
import './info.dart';

class ChatGroup extends StatefulWidget {
  final User user;
  final Group group;

  ChatGroup({Key key, @required this.user, @required this.group})
      : super(key: key);
  @override
  _ChatGroupState createState() => _ChatGroupState();
}

class _ChatGroupState extends State<ChatGroup> {
  User get _user => widget.user;
  Group get _group => widget.group;

  Map<String, dynamic> _members = Map();
  List<DocumentSnapshot> listMessages = List();

  bool isInputEmpty = true;
  bool isShowingStickers = false;
  final TextEditingController inputController = TextEditingController();
  final FocusNode focusNode = FocusNode();

  bool isLoading = true;

  String annName = '...';
  String annImage =
      'http://bestnycacupuncturist.com/wp-content/uploads/2016/11/anonymous-avatar-sm.jpg';

  @override
  void initState() {
    super.initState();

    loading();

    inputController.addListener(() {
      if (inputController.text.trim() == '') {
        setState(() {
          isInputEmpty = true;
        });
      } else {
        setState(() {
          isInputEmpty = false;
        });
      }
    });

    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        setState(() {
          isShowingStickers = false;
        });
      }
    });

    _group.members.forEach((mem) async {
      var temp = await refs.getUser(id: mem).get();

      setState(() {
        _members[mem] = temp;
      });
    });
  }

  // Build UI
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              buildListMessages(),
              (isShowingStickers ? buildStickerPicker() : Container()),
              buildInput(),
            ],
          ),
        ],
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: Text(_group.groupName),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.info,
          ),
          onPressed: () {
            Info.groupInfo(context: context, user: _user, group: _group, members: _members);
          },
        ),
      ],
      bottom: PreferredSize(
        child: Divider(
          height: 0,
          color: Colors.blue,
        ),
        preferredSize: Size.fromHeight(0),
      ),
      centerTitle: true,
      elevation: 0.0,
      bottomOpacity: 0.5,
    );
  }

  Widget buildListMessages() {
    return Expanded(
      child: isLoading
          ? Center(
              child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.blue)))
          : StreamBuilder(
              stream: refs
                  .getGroupChatMessage(id: _group.groupId)
                  .orderBy('time', descending: true)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                    ),
                  );
                } else {
                  listMessages = snapshot.data.documents;

                  return ListView.builder(
                    itemBuilder: (context, index) {
                      if (listMessages[index]['user'] == _user.username) {
                        return MessageFactory.me(
                          msg: listMessages[index].data,
                          isLast: isLastInChain(index),
                        );
                      }

                      return (_members[listMessages[index]['user']] != null)
                          ? MessageFactory.peer(
                              name: _members[listMessages[index]['user']]
                                  ['name'],
                              avatar: _members[listMessages[index]['user']]
                                  ['avatar'],
                              isFirst: isFirstInChain(index),
                              isLast: isLastInChain(index),
                              msg: listMessages[index].data)
                          : MessageFactory.peer(
                              name: '(${listMessages[index]['user']})',
                              avatar: annImage,
                              isFirst: isFirstInChain(index),
                              isLast: isLastInChain(index),
                              msg: listMessages[index].data);
                    },
                    itemCount: listMessages.length,
                    reverse: true,
                    padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                  );
                }
              },
            ),
    );
  }

  Widget buildStickerPicker() {
    return Container(
      child: Wrap(
        spacing: 4.0,
        runSpacing: 4.0,
        children: qoobeeStickers.map((item) {
          return FlatButton(
            onPressed: () {
              sendMessage(content: item, type: 1);
            },
            child: Image.asset(
              'assets/stickers/$item',
              width: 48.0,
            ),
          );
        }).toList(),
      ),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: Colors.grey[400],
            width: 0.5,
          ),
        ),
      ),
      padding: EdgeInsets.all(8.0),
    );
  }

  Widget buildInput() {
    return Container(
      child: Row(
        children: <Widget>[
          // Material(
          //   child: Container(
          //     margin: EdgeInsets.symmetric(horizontal: 1.0),
          //     child: IconButton(
          //       icon: Icon(Icons.image),
          //       onPressed: () {},
          //       color: Colors.blue,
          //     ),
          //   ),
          //   color: Colors.white,
          // ),
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.0),
              child: IconButton(
                icon: Icon(Icons.announcement),
                onPressed: () {
                  setState(() {
                    isShowingStickers = !isShowingStickers;
                  });
                },
                color: Colors.blue,
              ),
            ),
            color: Colors.white,
          ),
          Flexible(
            child: Container(
              child: TextField(
                style: TextStyle(
                  fontSize: 16.0,
                ),
                decoration: InputDecoration.collapsed(
                    hintText: ' Nhập nội dung tin nhắn ...',
                    hintStyle: TextStyle(color: Colors.grey[300])),
                focusNode: focusNode,
                controller: inputController,
              ),
            ),
          ),
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.0),
              child: IconButton(
                icon: Icon(Icons.send),
                onPressed: (isInputEmpty)
                    ? null
                    : () => sendMessage(content: inputController.text, type: 0),
                color: Colors.blue,
              ),
            ),
            color: Colors.white,
          ),
        ],
      ),
      width: double.infinity,
      height: 50.0,
      decoration: BoxDecoration(
        border: Border(
            top: BorderSide(
          color: Colors.blue,
          width: 0.2,
        )),
        color: Colors.white,
      ),
    );
  }

  // Actions
  void loading() async {
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      isLoading = false;
    });
  }

  void sendMessage({String content, int type}) {
    inputController.clear();

    int now = DateTime.now().millisecondsSinceEpoch;

    refs
        .getGroupChatMessage(id: _group.groupId)
        .document(now.toString())
        .setData({
      'user': _user.username,
      'type': type,
      'content': content,
      'time': now,
    });
  }

  Widget handleNullUser(String id) {
    return Container();
  }

  bool isLastInChain(int index) {
    return (index - 1 >= 0)
        ? (listMessages[index].data['user'] !=
                listMessages[index - 1].data['user']
            ? true
            : false)
        : true;
  }

  bool isFirstInChain(int index) {
    return (index + 1 < listMessages.length)
        ? (listMessages[index].data['user'] !=
                listMessages[index + 1].data['user']
            ? true
            : false)
        : true;
  }
}
