import 'package:flutter/material.dart';
import 'package:u_share/dto/message_list_item.dart';
import 'package:u_share/utils/utils.dart';
import 'package:u_share/screens/main/message/chat_single.dart';
import 'package:u_share/screens/main/message/chat_group.dart';
import 'package:u_share/screens/main/message/search.dart';
import 'package:u_share/db/firestore_refs.dart' as refs;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:u_share/repositories/user_repository.dart';
import 'package:u_share/dto/user.dart';
import 'package:u_share/dto/group.dart';

class MessageScreen extends StatefulWidget {
  final UserRepository userRepository;

  const MessageScreen({Key key, @required this.userRepository})
      : super(key: key);

  @override
  _MessageScreenState createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  UserRepository get _userRepository => widget.userRepository;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tin nhắn'),
        backgroundColor: TabHelper.color(TabItem.message),
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            tooltip: 'Tìm kiếm',
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      UserSearchPage(userRepository: _userRepository),
                ),
              );
            },
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return StreamBuilder<QuerySnapshot>(
      stream: refs
          .getGroupsByUsername(username: _userRepository.currentUser.username)
          .snapshots(),
      builder: (context, groupsSnapshot) {
        return StreamBuilder<QuerySnapshot>(
            stream: refs.getAllUsers().snapshots(),
            builder: (context, usersSnapshot) {
              if (!usersSnapshot.hasData || !groupsSnapshot.hasData)
                return LinearProgressIndicator();

              if (groupsSnapshot.data == null || usersSnapshot.data == null)
                return LinearProgressIndicator();
              
              List<MessageListItem> items = usersSnapshot.data.documents.map((u) => MessageListItem.userItem(u.data)).toList();
              items.addAll(groupsSnapshot.data.documents.map((g) => MessageListItem.groupItem(g.data)));
              items.sort((x, y) => x.title.compareTo(y.title));

              return _buildListWithUsersAndGroup(context, items);
            });
      },
    );
  }

  Widget _buildListWithUsersAndGroup(
      BuildContext context,
      List<MessageListItem> items) {
    return ListView.separated(
      itemCount: items.length,
      itemBuilder: (context, position) {
        if (items[position].type == MessageItemType.user) {
          if(items[position].data['username'] == _userRepository.currentUser.username)
            return Container();

          List<dynamic> pinnedList = items[position].data['pinned'];

          if(pinnedList == null)
            return Container();
          
          if(!pinnedList.contains(_userRepository.currentUser.username))
            return Container();
        }
        return ListTile(
            title: Text(
              items[position].title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              items[position].subTitle,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
            leading: CircleAvatar(
              backgroundImage:
                  NetworkImage(items[position].image),
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                items[position].type == MessageItemType.group ? Icon(Icons.group) : Container(),
                SizedBox(width: 8.0),
                Icon(Icons.keyboard_arrow_right),
              ],
            ),
            onTap: () {
              Navigator.push(
                context,
                items[position].type == MessageItemType.group ? 
                MaterialPageRoute(
                  builder: (context) => ChatGroup(
                        user: _userRepository.currentUser,
                        group: Group.fromJson(items[position].data),
                      ),
                ) : MaterialPageRoute(
                  builder: (context) => ChatSingle(
                        user: _userRepository.currentUser,
                        peer: User.fromJson(items[position].data),
                      ),
                ),
              );
            },
          );
      },
      separatorBuilder: (context, position) {
        return Divider(
          height: 0,
        );
      },
    );
  }
}
