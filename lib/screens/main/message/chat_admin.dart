import 'package:flutter/material.dart';
import 'package:u_share/dto/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:u_share/db/firestore_refs.dart' as refs;
import 'package:u_share/screens/main/message/message_factory.dart';

class ChatAdmin extends StatefulWidget {
  final User user;

  ChatAdmin({Key key, @required this.user})
      : super(key: key);

  @override
  _ChatAdminState createState() => _ChatAdminState();
}

class _ChatAdminState extends State<ChatAdmin> {
  User get _user => widget.user;

  String w2mChatId = '';
  List<DocumentSnapshot> listMessages = List();

  bool isInputEmpty = true;
  final TextEditingController inputController = TextEditingController();

  @override
  void initState() {
    super.initState();

    getP2PChatId();

    inputController.addListener(() {
      if (inputController.text.trim() == '') {
        setState(() {
          isInputEmpty = true;
        });
      } else {
        setState(() {
          isInputEmpty = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              buildListMessages(),
              buildInput(),
            ],
          ),
        ],
      ),
    );
  }

  // Build UI
  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: Text('Trợ lý sinh viên'),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      centerTitle: true,
      elevation: 0.0,
      bottomOpacity: 0.5,
    );
  }

  Widget buildListMessages() {
    return Expanded(
      child: w2mChatId == ''
          ? Center(
              child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.blue)))
          : StreamBuilder(
              stream: refs
                  .getAdminChatMessages(id: w2mChatId)
                  .orderBy('time', descending: true)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return (Center(
                      child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.blue))));
                } else {
                  listMessages = snapshot.data.documents;

                  return ListView.builder(
                    itemBuilder: (context, index) {
                      return (listMessages[index].data['from'] ==
                              _user.username)
                          ? MessageFactory.me(
                              msg: listMessages[index].data,
                              isLast: isLastInChain(index))
                          : MessageFactory.peer(
                              name: 'Admin',
                              avatar: 'https://api.liberzy.com/images/place/Uxa1vpBBANqVU1NmHqv0H3bUdAuTsmkc.jpeg',
                              msg: listMessages[index].data,
                              isFirst: isFirstInChain(index),
                              isLast: isLastInChain(index),
                            );
                    },
                    itemCount: listMessages.length,
                    reverse: true,
                    padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                  );
                }
              },
            ),
    );
  }

  Widget buildInput() {
    return Container(
      child: Row(
        children: <Widget>[
          Flexible(
            child: Container(
              padding: EdgeInsets.only(left: 12.0),
              child: TextField(
                style: TextStyle(
                  fontSize: 16.0,
                ),
                decoration: InputDecoration.collapsed(
                    hintText: ' Nhập nội dung tin nhắn ...',
                    hintStyle: TextStyle(color: Colors.grey[300])),
                controller: inputController,
              ),
            ),
          ),
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.0),
              child: IconButton(
                icon: Icon(Icons.send),
                onPressed: (isInputEmpty || w2mChatId == '')
                    ? null
                    : () => sendMessage(content: inputController.text),
                color: Colors.blue,
              ),
            ),
            color: Colors.white,
          ),
        ],
      ),
      width: double.infinity,
      height: 50.0,
      decoration: BoxDecoration(
        border: Border(
            top: BorderSide(
          color: Colors.blue,
          width: 0.2,
        )),
        color: Colors.white,
      ),
    );
  }

  // Actions
  void getP2PChatId() async {
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      w2mChatId = 'ad:${_user.username}';
    });
  }

  void sendMessage({String content}) {
    inputController.clear();

    int now = DateTime.now().millisecondsSinceEpoch;

    refs.getAdminChatMessages(id: w2mChatId).document(now.toString()).setData({
      'from': _user.username,
      'to': 'admin',
      'content': content,
      'time': now,
      'type': 0
    });
  }

  bool isLastInChain(int index) {
    return (index - 1 >= 0)
        ? (listMessages[index].data['from'] !=
                listMessages[index - 1].data['from']
            ? true
            : false)
        : true;
  }

  bool isFirstInChain(int index) {
    return (index + 1 < listMessages.length)
        ? (listMessages[index].data['from'] !=
                listMessages[index + 1].data['from']
            ? true
            : false)
        : true;
  }
}
