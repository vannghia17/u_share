import 'package:flutter/material.dart';
import 'package:u_share/repositories/user_repository.dart';
import 'package:u_share/db/firestore_refs.dart' as refs;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:u_share/dto/user.dart';
import 'package:u_share/dto/group.dart';
import 'package:u_share/screens/main/message/chat_admin.dart';
import 'package:u_share/screens/main/message/chat_single.dart';
import 'package:u_share/screens/main/message/chat_group.dart';

class UserSearchPage extends StatefulWidget {
  final UserRepository userRepository;

  const UserSearchPage({Key key, @required this.userRepository})
      : super(key: key);

  @override
  _UserSearchPageState createState() => _UserSearchPageState();
}

class _UserSearchPageState extends State<UserSearchPage>
    with SingleTickerProviderStateMixin {
  UserRepository get _userRepository => widget.userRepository;

  final List<Tab> tabs = [
    Tab(text: 'Người dùng'),
    Tab(text: 'Nhóm'),
  ];

  TabController _tabController;
  TextEditingController _searchQuery;

  String queryString = '';
  bool isInputEmpty = true;

  @override
  void initState() {
    super.initState();
    _searchQuery = TextEditingController();
    _searchQuery.addListener(() {
      setState(() {
        isInputEmpty = _searchQuery.text.isNotEmpty ? false : true;
      });
    });
    _tabController = TabController(vsync: this, length: tabs.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  bool isValid(String str) {
    if (queryString.isEmpty) return true;

    if (str.toLowerCase().contains(queryString.toLowerCase())) return true;

    return false;
  }

  Widget _buildSearchField() {
    return TextField(
      controller: _searchQuery,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Nhập từ khóa tìm kiếm...',
        border: InputBorder.none,
        hintStyle: TextStyle(color: Colors.white30),
      ),
      style: TextStyle(
        color: Colors.white,
        fontSize: 16.0,
      ),
    );
  }

  List<Widget> _buildAction() {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.search),
        onPressed: () {
          setState(() {
            queryString = _searchQuery.text;
          });
        },
      ),
      isInputEmpty
          ? Container()
          : IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                _searchQuery.clear();
                setState(() {
                  queryString = '';
                });
              },
            ),
    ];
  }

  TabBar _buildBottom() {
    return TabBar(
      controller: _tabController,
      tabs: this.tabs,
      indicator: BoxDecoration(color: Colors.blue[300]),
    );
  }

  Widget _users() {
    return StreamBuilder<QuerySnapshot>(
      stream: refs.getAllUsers().orderBy('name').snapshots(),
      builder: (context, usersSnapshot) {
        if (!usersSnapshot.hasData) return CircularProgressIndicator();
        if (usersSnapshot.data == null) return CircularProgressIndicator();

        return ListView.separated(
          itemCount: usersSnapshot.data.documents.length,
          itemBuilder: (context, position) {
            if (usersSnapshot.data.documents[position]['username'] ==
                _userRepository.currentUser.username) return Container();
            return isValid(usersSnapshot.data.documents[position]['name'])
                ? ListTile(
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage(
                          usersSnapshot.data.documents[position]['avatar']),
                    ),
                    title: Text(usersSnapshot.data.documents[position]['name']),
                    onTap: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ChatSingle(
                                user: _userRepository.currentUser,
                                peer: User.fromJson(usersSnapshot
                                    .data.documents[position].data),
                              ),
                        ),
                      );
                    },
                  )
                : Container();
          },
          separatorBuilder: (context, position) {
            return Divider(height: 0);
          },
        );
      },
    );
  }

  Widget _groups() {
    return StreamBuilder(
      stream: refs.getAllGroups().orderBy('groupName').snapshots(),
      builder: (context, groupsSnapshot) {
        if (!groupsSnapshot.hasData) return CircularProgressIndicator();
        if (groupsSnapshot.data == null) return CircularProgressIndicator();

        return ListView.separated(
          itemCount: groupsSnapshot.data.documents.length,
          itemBuilder: (context, position) {
            if (isValid(groupsSnapshot.data.documents[position]['groupName'])) {
              if (!groupsSnapshot.data.documents[position]['members']
                  .contains(_userRepository.currentUser.username)) {
                return ListTile(
                  enabled: false,
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(
                        groupsSnapshot.data.documents[position]['groupImage']),
                  ),
                  title: Text(
                    groupsSnapshot.data.documents[position]['groupName'],
                  ),
                  trailing: Text(
                    'Không có quyền truy cập',
                    style: TextStyle(
                      color: Colors.red,
                      fontSize: 12.0,
                    ),
                  ),
                  onTap: () {},
                );
              }

              return ListTile(
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(
                      groupsSnapshot.data.documents[position]['groupImage']),
                ),
                title:
                    Text(groupsSnapshot.data.documents[position]['groupName']),
                onTap: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChatGroup(
                            user: _userRepository.currentUser,
                            group: Group.fromJson(
                                groupsSnapshot.data.documents[position].data),
                          ),
                    ),
                  );
                },
              );
            }

            return Container();
          },
          separatorBuilder: (context, position) {
            return Divider(height: 0);
          },
        );
      },
    );
  }

  Widget _buildBody() {
    return TabBarView(
      physics: NeverScrollableScrollPhysics(),
      controller: _tabController,
      children: <Widget>[
        _users(),
        _groups(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _buildSearchField(),
        actions: _buildAction(),
        bottom: _buildBottom(),
        elevation: 0.0,
      ),
      body: _buildBody(),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ChatAdmin(
                    user: _userRepository.currentUser,
                  ),
            ),
          );
        },
        label: Text('TLSV'),
        icon: Icon(Icons.assistant),
        backgroundColor: Colors.blue,
      ),
    );
  }
}
