import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class MessageFactory {
  static Widget me({bool isLast = false, Map<String, dynamic> msg}) {
    var time = DateTime.fromMillisecondsSinceEpoch(msg['time']);

    if (msg['type'] == 0) {
      // Text Message
      return Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                top: 2.0, bottom: 2.0, right: 8.0, left: 128.0),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text(msg['content']),
                  isLast == true
                      ? Padding(
                          padding: const EdgeInsets.only(top: 4.0),
                          child: Text(
                            '${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}',
                            style: TextStyle(
                                color: Colors.grey[500], fontSize: 12.0),
                          ),
                        )
                      : Container(
                          width: 0,
                          height: 0,
                        ),
                ],
              ),
              padding: EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                color: Colors.grey[200],
                border: Border.all(
                  color: Colors.grey[400],
                  width: 0.5,
                ),
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(4.0),
                    topLeft: Radius.circular(12.0),
                    bottomLeft: Radius.circular(12.0),
                    bottomRight: Radius.circular(12.0)),
              ),
            ),
          ),
        ],
      );
    } else if (msg['type'] == 1) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                top: 2.0, bottom: 2.0, right: 8.0, left: 128.0),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Image.asset(
                    'assets/stickers/${msg['content']}',
                    width: 96.0,
                  ),
                  isLast == true
                      ? Padding(
                          padding: const EdgeInsets.only(top: 4.0),
                          child: Text(
                            '${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}',
                            style: TextStyle(
                                color: Colors.grey[500], fontSize: 12.0),
                          ),
                        )
                      : Container(
                          width: 0,
                          height: 0,
                        ),
                ],
              ),
              padding: EdgeInsets.all(8.0),
            ),
          ),
        ],
      );
    } else if (msg['type'] == 2) {
      return Container();
    }

    return Container();
  }

  static Widget peer(
      {String name,
      String avatar,
      bool isFirst = false,
      bool isLast = false,
      Map<String, dynamic> msg}) {
    var time = DateTime.fromMillisecondsSinceEpoch(msg['time']);

    if (msg['type'] == 0) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                top: 2.0, bottom: 2.0, right: 64.0, left: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                isFirst
                    ? Padding(
                        padding: const EdgeInsets.only(top: 8.0, right: 4.0),
                        child: CircleAvatar(
                          backgroundImage: CachedNetworkImageProvider(avatar),
                          radius: 16.0,
                        ),
                      )
                    : Container(
                        width: 36.0,
                      ),
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      isFirst
                          ? Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 4.0, top: 4.0),
                              child: Text(
                                name,
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 12.0,
                                ),
                              ),
                            )
                          : Container(),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              msg['content'],
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            isLast == true
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 4.0),
                                    child: Text(
                                      '${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}',
                                      style: TextStyle(
                                          color: Colors.grey[300],
                                          fontSize: 12.0),
                                    ),
                                  )
                                : Container(
                                    width: 0,
                                    height: 0,
                                  ),
                          ],
                        ),
                        padding: EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                          color: Colors.blue[700],
                          border: Border.all(
                            color: Colors.blue[400],
                            width: 0.5,
                          ),
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(12.0),
                              topLeft: Radius.circular(4.0),
                              bottomLeft: Radius.circular(12.0),
                              bottomRight: Radius.circular(12.0)),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    } else if (msg['type'] == 1) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                top: 2.0, bottom: 2.0, right: 64.0, left: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                isFirst
                    ? Padding(
                        padding: const EdgeInsets.only(top: 8.0, right: 4.0),
                        child: CircleAvatar(
                          backgroundImage: CachedNetworkImageProvider(avatar),
                          radius: 16.0,
                        ),
                      )
                    : Container(
                        width: 36.0,
                      ),
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      isFirst
                          ? Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 4.0, top: 4.0),
                              child: Text(
                                name,
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 12.0,
                                ),
                              ),
                            )
                          : Container(),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Image.asset(
                              'assets/stickers/${msg['content']}',
                              width: 96.0,
                            ),
                            isLast == true
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 4.0),
                                    child: Text(
                                      '${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}',
                                      style: TextStyle(
                                          color: Colors.grey[500],
                                          fontSize: 12.0),
                                    ),
                                  )
                                : Container(
                                    width: 0,
                                    height: 0,
                                  ),
                          ],
                        ),
                        padding: EdgeInsets.all(8.0),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    } else if (msg['type'] == 2) {
      return Container();
    } else if (msg['type'] == 5) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                top: 2.0, bottom: 2.0, right: 64.0, left: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                isFirst
                    ? Padding(
                        padding: const EdgeInsets.only(top: 8.0, right: 4.0),
                        child: CircleAvatar(
                          backgroundImage: CachedNetworkImageProvider(avatar),
                          radius: 16.0,
                        ),
                      )
                    : Container(
                        width: 36.0,
                      ),
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      isFirst
                          ? Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 4.0, top: 4.0),
                              child: Text(
                                name,
                                style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12.0,
                                ),
                              ),
                            )
                          : Container(),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              msg['content'],
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            isLast == true
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 4.0),
                                    child: Text(
                                      '${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}',
                                      style: TextStyle(
                                          color: Colors.grey[300],
                                          fontSize: 12.0),
                                    ),
                                  )
                                : Container(
                                    width: 0,
                                    height: 0,
                                  ),
                          ],
                        ),
                        padding: EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                          color: Colors.red[700],
                          border: Border.all(
                            color: Colors.red[400],
                            width: 0.5,
                          ),
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(12.0),
                              topLeft: Radius.circular(4.0),
                              bottomLeft: Radius.circular(12.0),
                              bottomRight: Radius.circular(12.0)),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    }

    return Container();
  }
}
