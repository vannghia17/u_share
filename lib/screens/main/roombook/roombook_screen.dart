import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:u_share/utils/utils.dart';
import 'package:u_share/db/firestore_refs.dart' as refs;
import 'package:u_share/repositories/user_repository.dart';
import 'package:u_share/dto/dto.dart';
import './building_block.dart';
import './noti_screen.dart';

class RoombookScreen extends StatefulWidget {
  final UserRepository userRepository;

  const RoombookScreen({Key key, @required this.userRepository})
      : super(key: key);

  @override
  _RoombookScreenState createState() => _RoombookScreenState();
}

class _RoombookScreenState extends State<RoombookScreen> {
  UserRepository get _userRepository => widget.userRepository;

  DateTime currentDate;
  DateTime choosenDate;
  List<String> shifts;
  int currentShift;

  List<RoomBooking> bookings;
  List<RoomBooking> bookingsByTime;

  @override
  void initState() {
    super.initState();

    DateTime temp = DateTime.now();

    currentDate = DateTime(temp.year, temp.month, temp.day);
    choosenDate = temp;

    shifts = [
      'Ca 1 (7:00 - 8:30)',
      'Ca 2 (8:45 - 10:15)',
      'Ca 3 (10:30 - 12:00)',
      'Ca 4 (13:00 - 14:30)',
      'Ca 5 (14:45 - 16:15)',
      'Ca 6 (16:30 - 18:00)'
    ];

    currentShift = 1;

    refs.getAllRoomBooking().snapshots().listen((snapshot) {
      setState(() {
        bookings = snapshot.documents
            .map((x) => RoomBooking.fromJson(x.data))
            .toList();

        bookingsByTime = bookings.where((roomBooking) {
          if (roomBooking.date.day == choosenDate.day &&
              roomBooking.date.month == choosenDate.month &&
              roomBooking.date.year == choosenDate.year &&
              roomBooking.shift == currentShift) return true;
          return false;
        }).toList();
      });
    });
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('Danh sách phòng họp'),
      backgroundColor: TabHelper.color(TabItem.roombook),
      elevation: 0.0,
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.notifications_active),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    BookingNotiScreen(userRepository: _userRepository),
              ),
            );
          },
        ),
      ],
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(56),
        child: Row(
          children: <Widget>[
            Expanded(
              child: FlatButton(
                onPressed: () async {
                  DateTime temp = await _selectDate();
                  setState(() {
                    if (temp != null) choosenDate = temp;

                    currentShift = 1;

                    bookingsByTime = bookings.where((roomBooking) {
                      if (roomBooking.date.day == choosenDate.day &&
                          roomBooking.date.month == choosenDate.month &&
                          roomBooking.date.year == choosenDate.year &&
                          roomBooking.shift == currentShift) return true;
                      return false;
                    }).toList();
                  });
                },
                textColor: Colors.yellow,
                child: Row(
                  children: <Widget>[
                    Icon(Icons.calendar_today, size: 14.0),
                    Text(
                      '  ${getDayString(choosenDate.weekday)}, ${choosenDate.day}/${choosenDate.month}/${choosenDate.year}',
                      style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.yellow,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Container(
                child: ButtonTheme(
                  alignedDropdown: true,
                  child: Theme(
                    data: ThemeData(
                        canvasColor: TabHelper.color(TabItem.roombook)),
                    child: DropdownButton<int>(
                      iconEnabledColor: Colors.yellow,
                      underline: Container(),
                      value: currentShift,
                      style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.yellow,
                      ),
                      onChanged: (int newShift) {
                        setState(() {
                          currentShift = newShift;

                          bookingsByTime = bookings.where((roomBooking) {
                            if (roomBooking.date.day == choosenDate.day &&
                                roomBooking.date.month == choosenDate.month &&
                                roomBooking.date.year == choosenDate.year &&
                                roomBooking.shift == currentShift) return true;
                            return false;
                          }).toList();
                        });
                      },
                      items: [1, 2, 3, 4, 5, 6]
                          .map<DropdownMenuItem<int>>((int shift) {
                        return DropdownMenuItem<int>(
                          value: shift,
                          child: Text(shifts[shift - 1]),
                        );
                      }).toList(),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildLoading() {
    return Center(child: CircularProgressIndicator());
  }

  Widget _buildBody() {
    return StreamBuilder<QuerySnapshot>(
      stream: refs.getAllBuildings().snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData || snapshot.data == null) return _buildLoading();

        List<Building> buildings = snapshot.data.documents
            .map<Building>((building) => Building.fromJson(building.data))
            .toList();

        return ListView.builder(
          itemCount: buildings.length,
          itemBuilder: (context, index) {
            return BuildingBlock(
              building: buildings[index],
              isOdd: index % 2 == 0,
              bookingsByTime: bookingsByTime,
              user: _userRepository.currentUser,
              date: choosenDate,
              shift: currentShift,
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      backgroundColor: Colors.white,
      body: _buildBody(),
    );
  }

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: choosenDate,
      firstDate: currentDate,
      lastDate: currentDate.add(Duration(days: 14)),
      locale: Locale.fromSubtags(languageCode: 'vi'),
    );
    return picked;
  }

  String getDayString(int day) {
    switch (day) {
      case 0:
        return 'Chủ Nhật';
      case 1:
        return 'Thứ hai';
      case 2:
        return 'Thứ ba';
      case 3:
        return 'Thứ tư';
      case 4:
        return 'Thứ năm';
      case 5:
        return 'Thứ sáu';
      case 6:
        return 'Thứ bảy';
    }
    return 'Chủ Nhật';
  }
}
