import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:u_share/utils/utils.dart';
import 'package:u_share/dto/dto.dart';
import 'package:u_share/screens/main/roombook/my_capcha.dart';

class RoomRequest extends StatefulWidget {
  final DateTime date;
  final int shift;
  final User user;
  final Room room;

  RoomRequest({Key key, this.date, this.shift, this.user, this.room})
      : super(key: key);

  @override
  _RoomRequestState createState() => _RoomRequestState();
}

class _RoomRequestState extends State<RoomRequest> {
  User get _user => widget.user;
  Room get _room => widget.room;
  DateTime get _date => widget.date;
  int get _shift => widget.shift;

  GlobalKey<ScaffoldState> scaffoldState = GlobalKey();

  List<String> shifts = [
    'Ca 1 (7:00 - 8:30)',
    'Ca 2 (8:45 - 10:15)',
    'Ca 3 (10:30 - 12:00)',
    'Ca 4 (13:00 - 14:30)',
    'Ca 5 (14:45 - 16:15)',
    'Ca 6 (16:30 - 18:00)'
  ];

  int currentStep;

  TextEditingController textController = TextEditingController();
  TextEditingController capchaController = TextEditingController();

  FocusNode focusNode = FocusNode();
  FocusNode capchaFocusNode = FocusNode();

  String textContent;
  String capchaString;

  bool isCapchaLoading = false;
  bool isSending = false;

  @override
  void initState() {
    super.initState();

    currentStep = 0;

    textContent = '';
    capchaString = '';

    isCapchaLoading = false;
    isSending = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      appBar: AppBar(
        title: Text('Đặt phòng họp'),
        backgroundColor: TabHelper.color(TabItem.roombook),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: _buildStepper(),
      ),
    );
  }

  Widget _buildStepper() {
    return Column(
      children: <Widget>[
        Container(
          child: Stepper(
            type: StepperType.vertical,
            currentStep: this.currentStep,
            onStepContinue: () {
              if (currentStep == 0) {
                if (textController.text.trim().isEmpty)
                  scaffoldState.currentState.showSnackBar(SnackBar(
                    content: Text('Nội dung không được để trống'),
                    backgroundColor: Colors.pink,
                    duration: Duration(seconds: 1),
                  ));
                else {
                  buildCapcha();
                }
              } else if (currentStep == 1) {
                if (!isCapchaLoading && capchaString != '') {
                  if (capchaController.text == capchaString) {
                    capchaFocusNode.unfocus();
                    setState(() {
                      currentStep = 2;
                    });
                  } else {
                    scaffoldState.currentState.showSnackBar(SnackBar(
                      content: Text('Capcha không chính xác'),
                      backgroundColor: Colors.pink,
                      duration: Duration(seconds: 1),
                    ));
                  }
                }
              } else if (currentStep == 2) {
                setState(() {
                  isSending = true;
                });

                Firestore.instance.collection('room_booking').add({
                  'user': _user.username,
                  'idRoom': _room.idRoom,
                  'shift': _shift,
                  'date': _date,
                  'status': 0,
                  'timeRequest': DateTime.now(),
                  'content': textContent,
                }).then((doc) {
                  showDialog(
                      context: context,
                      barrierDismissible: true,
                      builder: (context2) {
                        return AlertDialog(
                          title: Text('Gửi yêu cầu thành công'),
                          contentPadding: EdgeInsets.only(
                              left: 16.0, right: 16.0, bottom: 16.0),
                          titlePadding: EdgeInsets.all(16.0),
                          content: Text(
                              'Yêu cầu của bạn đã được gửi thành công và đang chờ duyệt.'),
                          actions: <Widget>[
                            FlatButton(
                              child: Text('Đóng'),
                              onPressed: () {
                                Navigator.pop(context2);
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        );
                      });

                  setState(() {
                    isSending = false;
                  });
                }).catchError((error) {
                  scaffoldState.currentState.showSnackBar(SnackBar(
                    content:
                        Text('Lỗi không xác định. Yêu cầu không thành công.'),
                    backgroundColor: Colors.pink,
                    duration: Duration(seconds: 1),
                  ));

                  setState(() {
                    isSending = false;
                  });
                });
              }
            },
            onStepCancel: () {
              if (currentStep == 0)
                Navigator.pop(context);
              else
                setState(() {
                  textController.text = '';
                  capchaController.text = '';
                  textContent = '';
                  isCapchaLoading = false;
                  capchaString = '';
                  currentStep = 0;
                });
            },
            physics: ClampingScrollPhysics(),
            controlsBuilder: (BuildContext context,
                {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
              return Padding(
                padding: const EdgeInsets.only(top: 12, right: 14, left: 14),
                child: Container(
                  child: Row(
                    children: <Widget>[
                      FlatButton(
                        color: TabHelper.color(TabItem.roombook),
                        textColor: Colors.white,
                        child: Text('Tiếp tục'),
                        onPressed: onStepContinue,
                      ),
                      SizedBox(width: 8.0),
                      FlatButton(
                        textColor: Colors.red,
                        child: Text('Quay lại'),
                        onPressed: onStepCancel,
                      ),
                    ],
                  ),
                ),
              );
            },
            steps: [
              buildStep1(),
              buildStep2(),
              buildStep3(),
            ],
          ),
        ),
        SizedBox(
          height: 32.0,
        ),
        isSending ? CircularProgressIndicator() : Container(),
      ],
    );
  }

  void buildCapcha() async {
    setState(() {
      currentStep = 1;
      isCapchaLoading = true;
      textContent = textController.text.trim();
    });
    focusNode.unfocus();
    String currentCapcha = Randomizer.getRandomString();
    setState(() {
      capchaString = currentCapcha;
    });
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      isCapchaLoading = false;
    });
  }

  Step buildStep1() {
    return Step(
      title: Text('Mục đích'),
      subtitle: Text('Nhập mục đích sử dụng phòng họp'),
      isActive: currentStep == 0,
      content: TextField(
        controller: textController,
        focusNode: focusNode,
        autofocus: true,
        maxLength: 200,
        minLines: 1,
        maxLines: 3,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.all(16.0),
            hintText: 'Nhập nội dung ...'),
        keyboardType: TextInputType.multiline,
      ),
    );
  }

  Step buildStep2() {
    return Step(
      title: Text('Xác minh Capcha'),
      isActive: currentStep == 1,
      content: Container(
        child: (isCapchaLoading || capchaString == '')
            ? CircularProgressIndicator()
            : Row(
                children: <Widget>[
                  MyCapcha(capcha: capchaString),
                  SizedBox(width: 8.0),
                  Expanded(
                    child: TextField(
                      controller: capchaController,
                      focusNode: capchaFocusNode,
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.text,
                      maxLength: 4,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                      maxLines: 1,
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  Step buildStep3() {
    return Step(
      title: Text("Xác nhận"),
      subtitle: Text('Kiểm tra lại thông tin và gửi yêu cầu'),
      isActive: currentStep == 2,
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text('Phòng: '),
                        Text(
                          _room.idRoom,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    SizedBox(height: 8.0),
                    Row(
                      children: <Widget>[
                        Text('Ngày: '),
                        Text(
                          '${_date.day}/${_date.month}/${_date.year}',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    SizedBox(height: 8.0),
                    Row(
                      children: <Widget>[
                        Text('Máy chiếu: '),
                        Text(
                          _room.projector ? 'Có' : 'Không',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text('Sức chứa: '),
                        Text(
                          _room.capacity.toString(),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    SizedBox(height: 8.0),
                    Row(
                      children: <Widget>[
                        Text('Ca: '),
                        Text(
                          shifts[_shift - 1],
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    SizedBox(height: 8.0),
                    Row(
                      children: <Widget>[
                        Text('Máy lạnh: '),
                        Text(
                          _room.airCondition ? 'Có' : 'Không',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 16.0),
          Text('Mục đích sử dụng phòng họp:', textAlign: TextAlign.left),
          SizedBox(height: 8.0),
          Padding(
            padding: EdgeInsets.only(left: 12),
            child: Container(
              padding: EdgeInsets.only(left: 8),
              decoration: BoxDecoration(
                border: Border(
                  left: BorderSide(
                    color: Colors.grey[300],
                    width: 8.0,
                  ),
                ),
              ),
              child: Text(
                textContent,
                style: TextStyle(
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
