import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:u_share/utils/utils.dart';
import 'package:u_share/db/firestore_refs.dart' as refs;
import 'package:u_share/repositories/user_repository.dart';
import 'package:u_share/dto/dto.dart';

class BookingNotiScreen extends StatelessWidget {
  final UserRepository userRepository;

  BookingNotiScreen({Key key, @required this.userRepository}) : super(key: key);

  final List<String> shifts = [
    'Ca 1 (7:00 - 8:30)',
    'Ca 2 (8:45 - 10:15)',
    'Ca 3 (10:30 - 12:00)',
    'Ca 4 (13:00 - 14:30)',
    'Ca 5 (14:45 - 16:15)',
    'Ca 6 (16:30 - 18:00)'
  ];

  Widget _buildBody() {
    return StreamBuilder<QuerySnapshot>(
      stream: refs
          .getAllBookingNoti(username: userRepository.currentUser.username)
          .orderBy('timeStamp', descending: true)
          .snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData || snapshot.data == null) {
          return Center(child: CircularProgressIndicator());
        }

        List<RoomBookingNoti> noties = snapshot.data.documents
            .map((x) => RoomBookingNoti.fromJson(x.data))
            .toList();

        return ListView.separated(
          itemCount: noties.length,
          itemBuilder: (context, index) {
            return Card(
              margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              elevation: 8.0,
              child: Container(
                padding: EdgeInsets.all(16.0),
                color: noties[index].success ? Colors.green[100] : Colors.red[100],
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        noties[index].success
                            ? Icon(Icons.check_circle_outline,
                                color: Colors.green[700])
                            : Icon(Icons.lock_outline, color: Colors.red[900]),
                        Text(
                          noties[index].success
                              ? '  Yêu cầu được phê duyệt'
                              : '  Yêu cầu bị từ chối',
                          style: TextStyle(
                            color: noties[index].success
                                ? Colors.green[700]
                                : Colors.red[900],
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 12.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Phòng: ${noties[index].idRoom}',
                          style: TextStyle(
                            color: noties[index].success
                                ? Colors.green[700]
                                : Colors.red[900],
                            fontSize: 14.0,
                          ),
                        ),
                        Text(
                          'Ngày: ${noties[index].date.day}/${noties[index].date.month}/${noties[index].date.year}',
                          style: TextStyle(
                            color: noties[index].success
                                ? Colors.green[700]
                                : Colors.red[900],
                            fontSize: 14.0,
                          ),
                        ),
                        Text(
                          '${shifts[noties[index].shift - 1]}',
                          style: TextStyle(
                            color: noties[index].success
                                ? Colors.green[700]
                                : Colors.red[900],
                            fontSize: 14.0,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 12.0),
                    Text(
                      'Chi tiết:',
                      style: TextStyle(
                        color: noties[index].success
                            ? Colors.green[700]
                            : Colors.red[900],
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 12.0),
                    Padding(
                      padding: EdgeInsets.only(left: 16.0),
                      child: Container(
                        padding: EdgeInsets.only(left: 8.0),
                        decoration: BoxDecoration(
                          border: Border(
                            left: BorderSide(
                              color: noties[index].success
                                  ? Colors.green[300]
                                  : Colors.red[400],
                              width: 4.0,
                            ),
                          ),
                        ),
                        child: Text(
                          noties[index].message,
                          style: TextStyle(color: Colors.grey[700]),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
          separatorBuilder: (context, index) {
            return Container();
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Kết quả đặt phòng họp'),
        backgroundColor: TabHelper.color(TabItem.roombook),
        elevation: 0.0,
      ),
      body: _buildBody(),
    );
  }
}
