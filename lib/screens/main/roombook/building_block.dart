import 'package:flutter/material.dart';
import 'package:u_share/dto/dto.dart';
import 'package:u_share/utils/utils.dart';
import './room_request.dart';

class BuildingBlock extends StatelessWidget {
  final Building building;
  final List<RoomBooking> bookingsByTime;
  final bool isOdd;
  final User user;
  final DateTime date;
  final int shift;

  BuildingBlock(
      {Key key,
      this.building,
      this.isOdd,
      this.bookingsByTime,
      this.user,
      this.date,
      this.shift})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final snackMePending = SnackBar(
      content: Text('Yêu cầu của bạn đang chờ duyệt'),
      duration: Duration(seconds: 1),
      backgroundColor: Colors.yellow[700],
    );
    final snackOtherPending = SnackBar(
      content: Text('Đang được đặt bởi người khác'),
      duration: Duration(seconds: 1),
      backgroundColor: Colors.yellow[700],
    );
    final snackSuccess = SnackBar(
      content: Text('Bạn đã đặt phòng thành công'),
      duration: Duration(seconds: 1),
      backgroundColor: Colors.green,
    );
    final snackFailed = SnackBar(
      content: Text('Phòng đã được người khác đặt'),
      duration: Duration(seconds: 1),
      backgroundColor: Colors.pink,
    );

    return Container(
      padding: EdgeInsets.symmetric(vertical: 18.0, horizontal: 36.0),
      color: isOdd ? Colors.grey[100] : Colors.white,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.only(right: 16.0),
              child: CircleAvatar(
                child: Text(building.idBuilding,
                    style: TextStyle(color: Colors.yellow)),
                radius: 20.0,
                backgroundColor: TabHelper.color(TabItem.roombook),
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: Container(
              child: Wrap(
                alignment: WrapAlignment.start,
                spacing: 8.0,
                children: building.rooms.map((room) {
                  RoomBooking tempBooking =
                      bookingsByTime.firstWhere((booking) {
                    return room.idRoom == booking.idRoom;
                  }, orElse: () => null);

                  return GestureDetector(
                    onTap: () {
                      if (tempBooking == null || tempBooking.status == -1) {
                        // Book room here
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                RoomRequest(date: date, shift: shift, user: user, room: room),
                          ),
                        );
                      } else {
                        if (tempBooking.status == 0 &&
                            tempBooking.user == user.username)
                          Scaffold.of(context).showSnackBar(snackMePending);
                        else if (tempBooking.status == 0 &&
                            tempBooking.user != user.username)
                          Scaffold.of(context).showSnackBar(snackOtherPending);
                        else if (tempBooking.status == 1 &&
                            tempBooking.user == user.username)
                          Scaffold.of(context).showSnackBar(snackSuccess);
                        else if (tempBooking.status == 1 &&
                            tempBooking.user != user.username)
                          Scaffold.of(context).showSnackBar(snackFailed);
                      }
                    },
                    child: Tooltip(
                      message: 'Sức chứa: ${room.capacity}',
                      child: Chip(
                        elevation: 4.0,
                        backgroundColor: tempBooking != null
                            ? (tempBooking.status == 0
                                ? Colors.yellow[700]
                                : (tempBooking.status == 1
                                    ? (tempBooking.user == user.username
                                        ? Colors.green
                                        : Colors.pink)
                                    : Colors.blue[300]))
                            : Colors.blue[300],
                        label: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              child: Text(
                                ' ' + room.idRoom,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                              width: 36,
                            ),
                            Opacity(
                              opacity: room.airCondition ? 1 : 0,
                              child: Icon(
                                Icons.ac_unit,
                                size: 16,
                                color: Colors.white,
                              ),
                            ),
                            Opacity(
                              opacity: room.projector ? 1 : 0,
                              child: Icon(
                                Icons.tv,
                                size: 16,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
