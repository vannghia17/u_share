import 'package:flutter/material.dart';
import 'package:u_share/utils/utils.dart';

class MyCapcha extends StatelessWidget {
  final String capcha;

  MyCapcha({this.capcha});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          _buildItem(capcha[0]),
          _buildItem(capcha[1]),
          _buildItem(capcha[2]),
          _buildItem(capcha[3]),
        ],
      ),
    );
  }

  Widget _buildItem(String c) {
    return Container(
      padding: EdgeInsets.all(10.0),
      color: Colors.grey[200],
      child: Text(
        c,
        style: TextStyle(
          fontSize: 24.0,
          fontWeight: FontWeight.bold,
          color: Randomizer.getRandomColors(),
        ),
      ),
    );
  }
}
