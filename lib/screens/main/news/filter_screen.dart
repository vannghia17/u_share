import 'package:flutter/material.dart';
import 'package:u_share/utils/utils.dart';
import './filter_object.dart';

class FilterScreen extends StatefulWidget {
  FilterScreen({Key key}) : super(key: key);

  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  DateTime _dateFrom;
  DateTime _dateTo;

  @override
  void initState() {
    super.initState();
    
    _dateFrom = null;
    _dateTo = null;
  }

  String getDateString(DateTime date) {
    if (date == null)
      return '(Chọn ngày)';
    else
      return '${date.day}/${date.month}/${date.year}';
  }

  Future _selectDateFrom() async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2019),
      lastDate: DateTime.now().add(Duration(minutes: 3)),
      locale: Locale.fromSubtags(languageCode: 'vi'),
    );
    return picked;
  }

  Future _selectDateTo() async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2019),
      lastDate: DateTime.now().add(Duration(minutes: 3)),
      locale: Locale.fromSubtags(languageCode: 'vi'),
    );
    return picked;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: TabHelper.color(TabItem.news),
        elevation: 0.0,
        title: Text('Lọc bảng tin'),
      ),
      body: Padding(
        padding: EdgeInsets.all(32.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              'Từ ngày:',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            FlatButton(
              onPressed: () async {
                DateTime temp = await _selectDateFrom();
                setState(() {
                  _dateFrom = temp;
                });
              },
              child: Text(getDateString(_dateFrom)),
              color: Colors.grey[200],
            ),
            SizedBox(height: 8),
            Text(
              'Đến ngày:',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            FlatButton(
              onPressed: () async {
                DateTime temp = await _selectDateTo();
                setState(() {
                  _dateTo = temp;
                });
              },
              child: Text(getDateString(_dateTo)),
              color: Colors.grey[200],
            ),
            SizedBox(height: 32),
            FlatButton(
              onPressed: () {
                Navigator.pop(
                  context,
                  new FilterObject(dateFrom: _dateFrom, dateTo: _dateTo),
                );
              },
              child: Text('Lọc'),
              color: TabHelper.color(TabItem.news),
              textColor: Colors.white,
            ),
          ],
        ),
      ),
    );
  }
}
