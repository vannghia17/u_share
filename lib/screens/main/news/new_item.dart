import 'package:flutter/material.dart';
import 'package:u_share/dto/dto.dart';
import 'package:cached_network_image/cached_network_image.dart';

class NewItem extends StatelessWidget {
  final New newItem;
  final Group group;
  final VoidCallback onTap;

  NewItem({Key key, this.newItem, this.group, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Card(
        elevation: 2.0,
        color: Colors.grey[50],
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(16.0)),
            side: BorderSide(width: 1, color: Colors.grey[200])),
        child: InkWell(
          splashColor: Colors.green.withAlpha(30),
          onTap: this.onTap,
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: CachedNetworkImageProvider(group.groupImage),
                  radius: 28.0,
                ),
                SizedBox(height: 8.0),
                Expanded(
                  child: Container(
                    child: Text(
                      newItem.title,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 32.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          newItem.isPublic
                              ? Padding(
                                  padding: EdgeInsets.only(right: 8.0),
                                  child: Icon(Icons.public, color: Colors.blue),
                                )
                              : Container(),
                          newItem.isImportant
                              ? Padding(
                                  padding: EdgeInsets.only(right: 8.0),
                                  child: Icon(Icons.report, color: Colors.red),
                                )
                              : Container(),
                        ],
                      ),
                      Text(
                          '${newItem.timeStamp.day}/${newItem.timeStamp.month}/${newItem.timeStamp.year}',
                          style: TextStyle(color: Colors.grey)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
