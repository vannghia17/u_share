import 'package:flutter/material.dart';
import 'package:u_share/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:u_share/dto/dto.dart';

class NewDetail extends StatelessWidget {
  final String avatarUrl;
  final String groupName;
  final New data;

  NewDetail(
      {Key key,
      @required this.avatarUrl,
      @required this.data,
      @required this.groupName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: TabHelper.color(TabItem.news),
            elevation: 0.0,
            title: Text(groupName),
            actions: [
              SizedBox(width: 8.0),
              data.isImportant ? Icon(Icons.report) : Container(),
              data.isPublic ? Icon(Icons.public) : Container(),
              SizedBox(width: 8.0),
            ]),
        body: Container(
          color: Colors.white,
          child: ListView(
              padding: EdgeInsets.symmetric(
                vertical: 24.0,
                horizontal: 23.0,
              ),
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                        child: CircleAvatar(
                          backgroundImage:
                              CachedNetworkImageProvider(avatarUrl),
                        ),
                        width: 80.0,
                        height: 80.0),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(left: 16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Text(
                              data.title,
                              style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(height: 4.0),
                            Text(
                              '${data.timeStamp.day}/${data.timeStamp.month}/${data.timeStamp.year}',
                              textAlign: TextAlign.right,
                              style: TextStyle(color: Colors.grey),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 32.0),
                Card(
                  elevation: 5.0,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      constraints: BoxConstraints(
                        minHeight: 240.0,
                      ),
                      child: Text(
                        data.content,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontSize: 15.0,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 16.0),
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    color: Colors.green,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Thời gian:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      getMoreInfo((data.time != null && data.time != '')
                          ? data.time
                          : 'Không có thông tin'),
                    ],
                  ),
                ),
                SizedBox(height: 8.0),
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    color: Colors.green,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Địa điểm:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      getMoreInfo((data.place != null && data.place != '')
                          ? data.place
                          : 'Không có thông tin'),
                    ],
                  ),
                ),
              ]),
        ));
  }

  Widget getMoreInfo(String str) {
    return Text(
      str,
      style: TextStyle(
        color: Colors.white,
      ),
    );
  }
}
