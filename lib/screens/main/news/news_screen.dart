import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:u_share/utils/utils.dart';
import 'package:u_share/db/firestore_refs.dart' as refs;
import 'package:u_share/repositories/user_repository.dart';
import 'package:u_share/dto/dto.dart';
import './new_detail.dart';
import './new_item.dart';
import './pinned_item.dart';
import './filter_screen.dart';
import './filter_object.dart';

class NewsScreen extends StatefulWidget {
  const NewsScreen({Key key, @required this.userRepository}) : super(key: key);

  final UserRepository userRepository;

  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _SliverHeaderDelegate extends SliverPersistentHeaderDelegate {
  final Widget child;

  double minHeight = 32;
  double maxHeight = 32;

  _SliverHeaderDelegate({this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }

  @override
  double get maxExtent => math.max(maxHeight, minHeight);

  @override
  double get minExtent => minHeight;

  @override
  bool shouldRebuild(_SliverHeaderDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}

class _NewsScreenState extends State<NewsScreen> {
  UserRepository get _userRepository => widget.userRepository;

  List<Group> allGroups;
  List<String> userGroups;

  FilterObject filterObj;

  @override
  void initState() {
    super.initState();

    filterObj = null;

    refs.getAllGroups().snapshots().listen((docs) {
      setState(() {
        allGroups =
            docs.documents.map((doc) => Group.fromJson(doc.data)).toList();

        userGroups = allGroups
            .where((doc) =>
                doc.members.contains(_userRepository.currentUser.username))
            .map((doc) => doc.groupId)
            .toList();
      });
    });
  }

  bool isUserIn(String id) {
    return userGroups.contains(id);
  }

  Group getInfoByGroupId(String id) {
    return allGroups.firstWhere((group) => group.groupId == id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bảng tin'),
        backgroundColor: TabHelper.color(TabItem.news),
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.filter_list),
            onPressed: () async {
              FilterObject filterResult = await Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => FilterScreen()),
              );

              setState(() {
                filterObj = filterResult;
              });
            },
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: userGroups != null ? _buildBody() : LinearProgressIndicator(),
    );
  }

  SliverPersistentHeader makeHeader(String text) {
    return SliverPersistentHeader(
      floating: false,
      pinned: false,
      delegate: _SliverHeaderDelegate(
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.grey,
            ),
          ),
        ),
      ),
    );
  }

  SliverPersistentHeader makeTextNotice(String text) {
    return SliverPersistentHeader(
      floating: false,
      pinned: false,
      delegate: _SliverHeaderDelegate(
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontStyle: FontStyle.italic,
              color: Colors.grey,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return StreamBuilder<QuerySnapshot>(
      stream: refs.getNews().orderBy('timeStamp', descending: true).snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData || snapshot.data == null)
          return LinearProgressIndicator();

        var importants = snapshot.data.documents
            .where((doc) {
              return (isUserIn(doc.data['groups']) ||
                      doc.data['isPublic'] == true)
                  ? doc.data['isImportant']
                  : false;
            })
            .map((doc) => New.fromJson(doc.data))
            .take(5)
            .toList();

        List<New> allNews = snapshot.data.documents
            .where((doc) {
              return (isUserIn(doc.data['groups']) ||
                      doc.data['isPublic'] == true)
                  ? true
                  : false;
            })
            .map((doc) => New.fromJson(doc.data))
            .where((doc) {
              if (filterObj != null) {
                if (filterObj.dateFrom != null) {
                  return doc.timeStamp.compareTo(filterObj.dateFrom) >= 0;
                }
              }
              return true;
            })
            .where((doc) {
              if (filterObj != null) {
                if (filterObj.dateTo != null) {
                  return doc.timeStamp.compareTo(filterObj.dateTo) <= 0;
                }
              }
              return true;
            })
            .toList();

        List<Widget> _res = List<Widget>();

        _res.add(makeHeader('Tin quan trọng'));
        importants.length > 0
            ? _res.add(_buildImportants(importants))
            : _res.add(makeTextNotice('Không có tin quan trọng'));

        _res.add(makeHeader('Tất cả thông báo'));
        allNews.length > 0
            ? _res.add(_buildNews(allNews))
            : _res.add(makeTextNotice('Không có thông báo'));

        return CustomScrollView(slivers: _res);
      },
    );
  }

  Widget _buildImportants(List<New> importantNews) {
    return SliverGrid.count(
      crossAxisCount: 5,
      children: importantNews.map((doc) {
        return Center(
          child: PinnedItem(
            imageUrl: getInfoByGroupId(doc.groups).groupImage,
            time: '${doc.timeStamp.day}/${doc.timeStamp.month}',
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                final groupsInfo = getInfoByGroupId(doc.groups);
                return NewDetail(
                    data: doc,
                    avatarUrl: groupsInfo.groupImage,
                    groupName: groupsInfo.groupName);
              }));
            },
          ),
        );
      }).toList(),
    );
  }

  Widget _buildNews(List<New> allNews) {
    return SliverGrid.count(
      crossAxisCount: 2,
      childAspectRatio: 0.9,
      children: allNews
          .map((item) => NewItem(
                newItem: item,
                group: getInfoByGroupId(item.groups),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    var groupsInfo = getInfoByGroupId(item.groups);
                    return NewDetail(
                        data: item,
                        avatarUrl: groupsInfo.groupImage,
                        groupName: groupsInfo.groupName);
                  }));
                },
              ))
          .toList(),
    );
  }
}
