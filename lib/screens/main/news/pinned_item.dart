import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class PinnedItem extends StatefulWidget {
  final String imageUrl, time;
  final VoidCallback onTap;

  PinnedItem({Key key, this.imageUrl, this.onTap, this.time})
      : super(key: key);

  @override
  _PinnedItemState createState() => _PinnedItemState();
}

class _PinnedItemState extends State<PinnedItem> {
  String get imageUrl => widget.imageUrl;
  String get time => widget.time;
  VoidCallback get onTap => widget.onTap;

  bool isTouch = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Listener(
          onPointerDown: (PointerDownEvent d) {
            setState(() {
              isTouch = true;
            });
          },
          onPointerUp: (PointerUpEvent d) {
            setState(() {
              isTouch = false;
            });
          },
          child: GestureDetector(
            onTap: this.onTap,
            child: Container(
              child: CircleAvatar(
                backgroundImage: CachedNetworkImageProvider(imageUrl),
                radius: isTouch ? 20 : 24,
              ),
              decoration: BoxDecoration(
                  color: isTouch ? Colors.green[200] : Colors.green,
                  shape: BoxShape.circle),
              padding: const EdgeInsets.all(3.0),
            ),
          ),
        ),
        SizedBox(height: 4.0),
        Text(
          time,
          style: TextStyle(
            color: Colors.grey[700],
            fontSize: 12.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}
