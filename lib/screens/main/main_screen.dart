import 'package:flutter/material.dart';
import 'package:u_share/repositories/user_repository.dart';
import 'package:u_share/utils/utils.dart';
import 'package:u_share/screens/main/message/message_screen.dart';
import 'package:u_share/screens/main/news/news_screen.dart';
import 'package:u_share/screens/main/roombook/roombook_screen.dart';
import 'package:u_share/screens/main/setting/setting_screen.dart';

class MainScreen extends StatefulWidget {
  final UserRepository userRepository;

  MainScreen({Key key, @required this.userRepository}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  TabItem _currentTab = TabItem.message;

  UserRepository get _userRepository => widget.userRepository;

  void _selectTab(TabItem tabItem) {
    setState(() {
      _currentTab = tabItem;
    });
  }

  BottomNavigationBarItem _getBottomBarItem(TabItem tabItem) {
    String text = TabHelper.description(tabItem);
    IconData icon = TabHelper.icon(tabItem);

    return BottomNavigationBarItem(
      icon: Icon(
        icon,
        color: _currentTab == tabItem ? TabHelper.color(tabItem) : Colors.grey,
      ),
      title: Text(
        text,
        style: TextStyle(
          color:
              _currentTab == tabItem ? TabHelper.color(tabItem) : Colors.grey,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // body: _getScreen(_currentTab),
      body: IndexedStack(
        index: TabHelper.index(_currentTab),
        children: <Widget>[
          MessageScreen(userRepository: _userRepository),
          NewsScreen(userRepository: _userRepository),
          RoombookScreen(userRepository: _userRepository,),
          SettingScreen(userRepository: _userRepository),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        elevation: 0.0,
        items: [
          _getBottomBarItem(TabItem.message),
          _getBottomBarItem(TabItem.news),
          _getBottomBarItem(TabItem.roombook),
          _getBottomBarItem(TabItem.setting),
        ],
        currentIndex: TabHelper.index(_currentTab),
        onTap: (index) => _selectTab(TabHelper.item(index)),
      ),
    );
  }
}
