import 'package:flutter/material.dart';

class InfoDialog {
  var _arr = [
    {'name': 'Nguyễn Văn Nghĩa', 'mssv': '1512348'},
    {'name': 'Chu Phúc Nguyên', 'mssv': '1512353'},
    {'name': 'Hồ Thiện Nhân', 'mssv': '1512365'},
    {'name': 'Nguyễn Tấn Phát', 'mssv': '1512394'}
  ];

  List _buildArrWidgets() {
    return _arr.map((x) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              x['name'],
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(x['mssv']),
          ],
        ),
      );
    }).toList();
  }

  infomation(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Tác giả'),
            content: SingleChildScrollView(
              child: ListBody(
                children: _buildArrWidgets(),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.pop(context),
                child: Text('Đóng'),
              ),
            ],
          );
        });
  }
}
