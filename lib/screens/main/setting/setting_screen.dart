import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:u_share/utils/utils.dart';
import 'package:u_share/blocs/authentication/authentication.dart';
import 'package:u_share/repositories/user_repository.dart';
import 'package:u_share/dto/user.dart';
import 'package:u_share/screens/main/setting/info_dialog.dart';
import 'package:u_share/screens/main/setting/password_screen.dart';
import 'package:firebase_storage/firebase_storage.dart';

class SettingScreen extends StatefulWidget {
  final UserRepository userRepository;

  const SettingScreen({Key key, @required this.userRepository})
      : super(key: key);

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  UserRepository get _userRepository => widget.userRepository;

  User _user;

  bool isLoading = false;
  File avatarImageFile;

  InfoDialog infoDialog = new InfoDialog();

  @override
  void initState() {
    super.initState();
    _user = _userRepository.currentUser;
  }

  Future getImage() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      setState(() {
        avatarImageFile = image;
        isLoading = true;
      });
      uploadFile();
    }
  }

  Future uploadFile() async {
    String fileName = _user.username;
    StorageReference reference =
        FirebaseStorage.instance.ref().child('avatars/' + fileName);
    StorageUploadTask uploadTask = reference.putFile(avatarImageFile);
    StorageTaskSnapshot storageTaskSnapshot;
    uploadTask.onComplete.then((value) {
      if (value.error == null) {
        storageTaskSnapshot = value;

        storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
          String newAvatar = downloadUrl;

          Firestore.instance
              .collection('users')
              .document(_user.username)
              .updateData({'avatar': newAvatar}).then((data) async {
            setState(() {
              _user.avatar = newAvatar;
              isLoading = false;
            });
          }).catchError((err) {
            setState(() {
              isLoading = false;
            });
          });
        }, onError: (err) {
          setState(() {
            isLoading = false;
          });
        });
      } else {
        setState(() {
          isLoading = false;
        });
      }
    }, onError: (err) {
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final AuthenticationBloc authenticationBloc =
        BlocProvider.of<AuthenticationBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Cá nhân'),
        backgroundColor: TabHelper.color(TabItem.setting),
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.info),
            onPressed: () {
              infoDialog.infomation(context);
            },
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              isLoading
                  ? Padding(
                      padding: EdgeInsets.only(top: 24),
                      child: CircularProgressIndicator(),
                    )
                  : GestureDetector(
                      onTap: getImage,
                      child: Container(
                        margin: EdgeInsets.only(top: 24.0),
                        width: 140.0,
                        height: 140.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(_user.avatar),
                          ),
                        ),
                      ),
                    ),
              SizedBox(
                height: 36.0,
              ),
              Container(
                padding: EdgeInsets.only(
                    top: 12.0, bottom: 12.0, left: 36.0, right: 36.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                      top: BorderSide(color: Colors.grey, width: 0.2),
                      bottom: BorderSide(color: Colors.grey, width: 0.2)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Username',
                      style: TextStyle(fontSize: 16.0),
                    ),
                    Text(
                      _user.username,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: 12.0, bottom: 12.0, left: 36.0, right: 36.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                      bottom: BorderSide(color: Colors.grey, width: 0.2)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Họ tên',
                      style: TextStyle(fontSize: 16.0),
                    ),
                    Text(
                      _user.name,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: 12.0, bottom: 12.0, left: 36.0, right: 36.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                      bottom: BorderSide(color: Colors.grey, width: 0.2)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Ngành',
                      style: TextStyle(fontSize: 16.0),
                    ),
                    Text(
                      _user.department,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: 12.0, bottom: 12.0, left: 36.0, right: 36.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                      bottom: BorderSide(color: Colors.grey, width: 0.2)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Giới tính',
                      style: TextStyle(fontSize: 16.0),
                    ),
                    Text(
                      _user.sex == 'male' ? 'Nam' : 'Nữ',
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 32.0,
              ),
              IntrinsicWidth(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    RaisedButton(
                      padding: EdgeInsets.symmetric(horizontal: 60.0),
                      color: Colors.blue,
                      textColor: Colors.white,
                      elevation: 0.0,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PasswordScreen(
                                  userRepository: _userRepository,
                                ),
                          ),
                        );
                      },
                      child: Text('Đổi mật khẩu'),
                    ),
                    RaisedButton(
                      color: Colors.red,
                      textColor: Colors.white,
                      elevation: 0.0,
                      onPressed: () {
                        authenticationBloc.dispatch(LoggedOut());
                      },
                      child: Text('Đăng xuất'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
