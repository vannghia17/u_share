import 'package:flutter/material.dart';
import 'package:u_share/repositories/user_repository.dart';
import 'package:u_share/db/firestore_refs.dart' as refs;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:crypto/crypto.dart';
import 'package:u_share/utils/tabs_helper.dart';
import 'dart:convert';

class PasswordScreen extends StatefulWidget {
  final UserRepository userRepository;

  const PasswordScreen({Key key, @required this.userRepository})
      : super(key: key);

  @override
  _PasswordScreenState createState() => _PasswordScreenState();
}

class _FormData {
  String currentPassword = '';
  String newPassword = '';
  String confirmPassword = '';
}

class _PasswordScreenState extends State<PasswordScreen> {
  UserRepository get _userRepository => widget.userRepository;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  String password;

  bool isLoadingPassword = true;
  bool isChanging = false;

  _FormData _data = _FormData();

  void submit(BuildContext context) {
    _formKey.currentState.save();

    setState(() {
      isChanging = true;
    });

    if (getMD5(_data.currentPassword) != password) {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text('Mật khẩu hiện tại không chính xác'),
          backgroundColor: Colors.red,
          duration: Duration(milliseconds: 1000),
        ),
      );

      setState(() {
        isChanging = false;
      });
    } else if (_data.newPassword != _data.confirmPassword) {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text('Mật khẩu nhập lại không chính xác'),
          backgroundColor: Colors.red,
          duration: Duration(milliseconds: 1000),
        ),
      );

      setState(() {
        isChanging = false;
      });
    } else {
      var docRef = refs.getUser(id: _userRepository.currentUser.username);

      Firestore.instance.runTransaction((transaction) async {
        await transaction.update(docRef, {
          'password': getMD5(_data.newPassword),
        });

        await Future.delayed(Duration(seconds: 1));

        password = getMD5(_data.newPassword);

        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text('Đổi mật khẩu thành công'),
            backgroundColor: Colors.green,
            duration: Duration(milliseconds: 1000),
          ),
        );

        _formKey.currentState.reset();

        setState(() {
          isChanging = false;
        });
      });
    }
  }

  String getMD5(String str) {
    var bytes = utf8.encode(str);
    return md5.convert(bytes).toString();
  }

  @override
  void initState() {
    super.initState();
    refs.getUser(id: _userRepository.currentUser.username).get().then((doc) {
      password = doc.data['password'];
      setState(() {
        isLoadingPassword = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Đổi mật khẩu'),
        backgroundColor: TabHelper.color(TabItem.setting),
        elevation: 0.0,
      ),
      body: isLoadingPassword
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(32.0),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      obscureText: true,
                      keyboardType: TextInputType.text,
                      onSaved: (String value) {
                        this._data.currentPassword = value;
                      },
                      decoration: InputDecoration(
                        hintText: 'Nhập mật khẩu hiện tại',
                        contentPadding: EdgeInsets.all(12.0),
                        border: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.black, width: 5.0),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(4.0),
                            topRight: Radius.circular(4.0),
                          ),
                        ),
                      ),
                    ),
                    TextFormField(
                      obscureText: true,
                      keyboardType: TextInputType.text,
                      onSaved: (String value) {
                        this._data.newPassword = value;
                      },
                      decoration: InputDecoration(
                        hintText: 'Nhập mật khẩu mới',
                        contentPadding: EdgeInsets.all(12.0),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.black,
                            width: 5.0,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(0)),
                        ),
                      ),
                    ),
                    TextFormField(
                      obscureText: true,
                      keyboardType: TextInputType.text,
                      onSaved: (String value) {
                        this._data.confirmPassword = value;
                      },
                      decoration: InputDecoration(
                        hintText: 'Xác nhận mật khẩu mới',
                        contentPadding: EdgeInsets.all(12.0),
                        border: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.black, width: 5.0),
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(4.0),
                            bottomRight: Radius.circular(4.0),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                    RaisedButton(
                      elevation: 0.0,
                      onPressed: isChanging
                          ? null
                          : () {
                              _formKey.currentState.reset();
                            },
                      child: Container(
                        child: Text(
                          'Đặt lại',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        width: double.infinity,
                      ),
                      color: Colors.yellow[700],
                    ),
                    RaisedButton(
                      elevation: 0.0,
                      onPressed: isChanging
                          ? null
                          : () {
                              this.submit(context);
                            },
                      child: Container(
                        child: Text(
                          'Đổi mật khẩu',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        width: double.infinity,
                      ),
                      color: Colors.blue[500],
                    ),
                    SizedBox(
                      height: 32.0,
                    ),
                    (isChanging ? CircularProgressIndicator() : Container()),
                  ],
                ),
              ),
            ),
    );
  }
}
