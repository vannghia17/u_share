# u_share

## Chức năng đặt phòng

### Các Collections liên quan

- **rooms**: Chứa thông tin các phòng được gom nhóm theo từng tòa nhà. Ví dụ:

````
[C] {
    idBuilding: C,
    rooms: [
        {
            idRoom: C32,
            capacity: 50, // Sức chứa
            airCondition: true, // Có máy lạnh không
            projector: false, // Có máy chiếu không
        },
        {
            ...
        }
    ],
}
````

- **room_booking**: Thông tin các yêu cầu đặt phòng. Ví dụ:

````
[auto-id] {
    user: 1512348, // Người đặt phòng
    idRoom: D33, // Phòng muốn đặt
    shift: 2, // Ca, xem chi tiết bên dưới
    status: 0 // Trạng thái, xem chi tiết bên dưới
    date: 6/10/2019 // Thời gian phòng muốn đặt, chỉ quan tâm ngày-tháng-năm, còn thời gian thì dựa vào ca
    content: "Họp CLB Bóng chuyền",
    timeRequest: ... // Thời gian gửi yêu cầu đặt phòng, dùng để sort
}
````

status: 0 (đang chờ duyệt), 1 (chấp nhận), -1 (từ chối)

shift là thời gian được cài cứng theo thời gian mỗi ca 90 phút. Từ shift (1,2,3,4,5,6) trong database map qua String (ví dụ ca 1 là "7h00-8h30") để hiển thị lên Web.

````
static const SHIFT_1_BEGIN = TimeOfDay(hour: 7, minute: 0);
static const SHIFT_1_END = TimeOfDay(hour: 8, minute: 30);

static const SHIFT_2_BEGIN = TimeOfDay(hour: 8, minute: 45);
static const SHIFT_2_END = TimeOfDay(hour: 10, minute: 15);

static const SHIFT_3_BEGIN = TimeOfDay(hour: 10, minute: 30);
static const SHIFT_3_END = TimeOfDay(hour: 12, minute: 0);

static const SHIFT_4_BEGIN = TimeOfDay(hour: 13, minute: 0);
static const SHIFT_4_END = TimeOfDay(hour: 14, minute: 30);

static const SHIFT_5_BEGIN = TimeOfDay(hour: 14, minute: 45);
static const SHIFT_5_END = TimeOfDay(hour: 16, minute: 15);

static const SHIFT_6_BEGIN = TimeOfDay(hour: 16, minute: 30);
static const SHIFT_6_END = TimeOfDay(hour: 18, minute: 00);
````

date chỉ quan tâm đến **ngày/tháng/năm** là ngày user muốn đặt phòng (không quan tâm giờ phút giây), còn đặt phòng từ giờ nào tới giờ nào thì dựa vào shift như trên.

- **room_booking_noti**: thông báo về kết quả duyệt đặt phòng mà bên Web sẽ cập nhật lên database:

````
[auto-id] {
    user: 1512348, // Người đặt phòng
    idRoom: D33, // Phòng đã đặt
    shift: 2, // Ca 2
    date: 10/6/2019, // Ngày muốn có phòng
    success: true, // Admin chấp nhận đặt phòng
    message: "BQT đã chấp nhận yêu cầu đặt phòng của bạn", // Kiểu như 1 tin nhắn để hiển thị bên mobile
    timeStamp: ... // Thời gian admin cập nhật cái này lên database
}
````

shift với date tương tự như phần **room_booking**

Nếu chấp nhận -> success = true và message = "BQT đã chấp nhận yêu cầu đặt phòng của bạn".
Nếu từ chối -> success = false và message: "Yêu cầu đặt phòng bị từ chối: Lý do: ..." (nếu từ chối thì hiện ô cho nhập lý do)

### Flow

1. Bên Web, ở mục "Duyệt các yêu cầu đặt phòng", query tất cả các Document trong Collection **room_booking** mà có status = 0 (tức đang chờ duyệt).
2. Nếu bấm **đồng ý** -> set lại status = 1, đồng thời thêm 1 JSON vào **room_booking_noti** theo định dạng trên.
3. Nếu bấm **từ chối** -> set lại status = -1, hiển thị ra 1 dialog nhập lý do từ chối rồi thêm 1 JSON vào **room_booking_noti** theo định dạng trên.